import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		loading_submit: false,
		loading: true,
        time_countdown: null,
        username: '',
		auth: false
	},
	getters: {
		convertToCurrency: state => number => {
			return new Intl.NumberFormat('id-ID').format(number)
		},
	},
	mutations: {
        CHECK_AUTH(state, data){
            state.auth = data.auth
			state.username = data.name
		}
	},
	actions: {
        getCheckAuth({ commit }){
			axios.get('/api/auth').then(res => {
                // console.log(res.data);
				commit('CHECK_AUTH', res.data)
			})
		},
	}
})