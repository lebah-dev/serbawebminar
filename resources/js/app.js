require('./bootstrap');

import { App, plugin } from '@inertiajs/inertia-vue'
import Vue from 'vue'

import store from './vuex/store.js'
import Toasted from 'vue-toasted'
import VueCoreVideoPlayer from 'vue-core-video-player'
import VueMeta from 'vue-meta'
import route from 'ziggy';
import { Ziggy } from './ziggy';
import axios from 'axios'
import VueAxios from 'vue-axios'



Vue.use(VueCoreVideoPlayer)
Vue.use(plugin)
Vue.use(Toasted)
Vue.use(VueMeta)
Vue.mixin({
    methods: {
        route: (name, params, absolute) => route(name, params, absolute, Ziggy),
    }
});
Vue.use(VueAxios, axios)


const el = document.getElementById('app')

new Vue({
  render: h => h(App, {
    props: {
      initialPage: JSON.parse(el.dataset.page),
      resolveComponent: name => require(`./Pages/${name}`).default,
    },
  }),
  store
}).$mount(el)