<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
    
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/fontello.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/fontello-codes.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/thumbs-embedded.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/color.css') }}">
    {{-- <script data-ad-client="ca-pub-7839987518006662" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> --}}

  </head>
  <body>
    @inertia
    <script src="{{ mix('/js/app.js') }}"></script>
    <script src="{{ mix('/js/template.js') }}"></script>
  </body>
</html>