@extends('layouts.admin.auth.default')

@section('title', 'Reset Password')

@section('loginContent')
<div class="login-form login-forgot">
	<form 
		class="form"
		method="POST" 
		action="{{ route('admin.password.email.send') }}">

		@csrf



		<div class="pb-13 pt-lg-0 pt-5">
			<h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Forgotten Password ?</h3>
			<p class="text-muted font-weight-bold font-size-h4">Enter your email to reset your password</p>


		</div>

		@if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

		<div class="form-group">

			<input 
				class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6 @error('email') is-invalid @enderror" 
				type="email" 
				placeholder="Email" 
				name="email"
				value="{{ old('email') }}" 
				autocomplete="off" />

			@error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
		</div>

		<div class="form-group d-flex flex-wrap pb-lg-0">
			<button type="submit" id="kt_login_forgot_submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-4">Submit</button>
		</div>

	</form>
</div>
@endsection