@extends('layouts.admin.auth.default')

@section('title', 'Login')

@section('loginContent')
	
	
	<div class="login-form login-signin">

		<form 
			class="form" 
			novalidate="novalidate" 
			action="{{ route('admin.login') }}"
			method="POST">

			@csrf
	
			<div class="pb-13 pt-lg-0 pt-5">
				<h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Welcome to SerbaWebinar</h3>
			</div>

			@if (session('message'))
		        <div class="alert alert-danger" style="width: 350px; height: auto;">
		            {{ session('message') }}
		        </div>
	    	@endif

	    	@if (session('message_verified'))
		        <div class="alert alert-custom alert-warning" role="alert" style="width: 350px; height: auto;">
		        	<div class="alert-text">
		        		{!! session('message_verified') !!} 	
		        	</div>
				    
				</div>
	    	@endif

			
	
			<div class="form-group">
				<label class="font-size-h6 font-weight-bolder text-dark">Email</label>
				<input 
					class="form-control form-control-solid h-auto py-6 px-6 rounded-lg @error('email') is-invalid @enderror" 
					type="email" 
					name="email" 
					autocomplete="off" />
					@error('email')
						<span class="invalid-feedback">
							{{ $message }}
						</span>
					@enderror
			</div>
	
			<div class="form-group">
				<div class="d-flex justify-content-between mt-n5">
					<label class="font-size-h6 font-weight-bolder text-dark pt-5">Password</label>
					<a href="{{route('admin.password.email.index')}}" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" id="kt_login_forgot">Forgot Password ?</a>
				</div>
				<input 
					class="form-control form-control-solid h-auto py-6 px-6 rounded-lg @error('password') is-invalid @enderror" 
					type="password" 
					name="password" 
					autocomplete="off" />
					@error('password')
						<span class="invalid-feedback">
							{{ $message }}
						</span>
					@enderror
			</div>
	
			<div class="pb-lg-0 pb-5">
				<button 
					type="submit" 
					class="btn btn-block btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">
					Sign In
				</button>
			</div>

		</form>
	</div>
@endsection