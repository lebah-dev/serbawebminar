@extends('layouts.admin.default')

@section('title', 'Create Playlist')
@section('pageTitle', 'Create Playlist')

@section('content')

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Create a New Playlist</h3>
	</div>

	<form 
		action="{{ route('admin.playlist.store') }}" 
		method="POST"
		enctype="multipart/form-data"
		class="form">

		@csrf

		<div class="card-body">
			<div class="form-group">
				<label>Name 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="name" 
					class="form-control @error('name') is-invalid @enderror"
					value="{{old('name')}}" 
					placeholder="Enter a new channel. " />
				@error('name')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Playlist For 
				<span class="text-danger">*</span></label>
				<select 
					name="channel_id"
					class="form-control select2 @error('channel_id') is-invalid @enderror" 
					id="playlistFor">
					@foreach($channels as $channel)
						<option></option>
			      		<option 
			      			value="{{ $channel->id }}"
			      			@if(collect(old('user_id'))->contains($channel->id))
			      				selected="selected" 
			      			@endif
			      		>
			      		{{ $channel->name }}
			      	</option>
			      	@endforeach
			     </select>
				@error('channel_id')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Privacy Setting 
				<span class="text-danger">*</span></label>
				<select 
					name="privacy_setting"
					class="form-control @error('privacy_setting') is-invalid @enderror">
					<option value="">--Pilih Privacy Setting--</option>
					<option value="public">Public</option>
					<option value="private">Private</option>
			     </select>
				@error('privacy_setting')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
		</div>
	</form>

</div>

@endsection

@push('scripts')
	<script>
		$(function() {
			$('#playlistFor').select2({
				placeholder: "Pilih Channel",
				allowClear: true,
			});
		});
	</script>
@endpush

