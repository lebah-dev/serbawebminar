<table class="table table-bordered">
	<tr>
		<th>Title</th>
		<td>{{ $playlist->name }}</td>
	</tr>
	<tr>
		<th>Privacy Setting</th>
		<td>
			@if( $playlist->privacy_setting === 'public')
				Public
			@else
				Private
			@endif
		</td>
	</tr>
	<tr>
		<th>Created at</th>
		<td>{{ $playlist->created_at->diffForhumans()}}</td>
	</tr>
	<tr>
		<th>Channel</th>
		<td>
			<table class="table table-bordered w-100">
				<tr>
					<th>Name</th>
					<th>Owner</th>
				</tr>
				<tr>
					<td>{{ $playlist->channel->name }}</td>
					<td>{{ $playlist->channel->user->name }}</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<th>List Video</th>
		<td>
			<table class="table table-bordered w-100">
				<tr>
					<th>Title</th>
					<th>Created at</th>
					<th>Owner</th>
				</tr>
				@foreach($playlist->videos as $video)
				<tr>
					<td>{{ $video->title }}</td>
					<td>{{ $video->created_at }}</td>
					<td>{{ $video->createBy->name}}</td>
				</tr>
				@endforeach
			</table>
		</td>
	</tr>

</table>