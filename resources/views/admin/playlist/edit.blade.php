@extends('layouts.admin.default')

@section('title', 'Edit Playlist')
@section('pageTitle', 'Edit Playlist')

@section('content')

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Edit Playlist</h3>
	</div>

	<form 
		action="{{ route('admin.playlist.update', $playlist->id) }}" 
		method="POST"
		enctype="multipart/form-data"
		class="form">

		@csrf
		@method('PUT')

		<div class="card-body">
			<div class="form-group">
				<label>Name 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="name" 
					class="form-control @error('name') is-invalid @enderror"
					value="{{old('name') ?? $playlist->name}}" 
					placeholder="Enter a new channel. " />
				@error('name')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Channel For 
				<span class="text-danger">*</span></label>
				<select 
					name="channel_id"
					class="form-control @error('channel_id') is-invalid @enderror"
					id="playlistFor"
					readonly="readonly"
					{{-- disabled=""  --}}
					>
			      		<option 
			      			value="{{ $playlist->channel_id }}"
			      			selected="selected">
			      		{{ $playlist->channel->name }}
			      	</option>
			     </select>
				@error('channel_id')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Privacy Setting 
				<span class="text-danger">*</span></label>
				<select 
					name="privacy_setting"
					class="form-control @error('privacy_setting') is-invalid @enderror">
					<option 
						value=""
						@if ((old('privacy_setting') == " ") || ($playlist->privacy_setting == '')) {{ 'selected' }} @endif>
						--Pilih Privacy Setting--
					</option>
					<option 
						value="public"
						@if ((old('privacy_setting') == "public") || ($playlist->privacy_setting == 'public')) {{ 'selected' }} @endif>
						Public
					</option>
					<option 
						value="private"
						@if ((old('privacy_setting') == "private") || ($playlist->privacy_setting == 'private')) {{ 'selected' }} @endif>
						Private
					</option>
			     </select>
				@error('privacy_setting')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
		</div>
	</form>

</div>

@endsection

@push('scripts')
	<script>
		$(function() {
			$('#playlistFor').select2({
				placeholder: "Pilih Channel",
				allowClear: true,
			});
		});
	</script>
@endpush

