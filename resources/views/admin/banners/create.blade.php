@extends('layouts.admin.default')

@section('title', 'Create Banner Ads')
@section('pageTitle', 'Create Banner Ads')

@section('content')

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Create a New Banner Ads</h3>
	</div>

	<form 
		action="{{ route('admin.banner.store') }}" 
		method="POST"
		enctype="multipart/form-data">

		@csrf

		<div class="card-body">
			<div class="form-group">
				<label>Name 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="name" 
					class="form-control @error('name') is-invalid @enderror"
					value="{{old('name')}}" 
					placeholder="Enter a new banner ads. " />
				@error('name')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Image 
				<span class="text-danger">*</span></label>
				<input 
					type="file"
					name="image" 
					class="form-control @error('image') is-invalid @enderror" 
					placeholder="Enter a new image." />
				<span class="form-text text-muted">
					Allowed File Extensions: .jpg, .jpeg, .png
				</span>
				<span class="form-text text-muted">
					For the best results on all devices, use an image that’s at least 1349 x 526 pixels and 2MB or less.
				</span>

				@error('image')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Description 
				<span class="text-danger">*</span></label>
				<textarea 
					class="form-control @error('description') is-invalid @enderror"
					rows="10" 
					name="description">{{old('description')}}</textarea>
				@error('description')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Gunakan Banner? 
				<span class="text-danger">*</span></label><br>
				<input 
					name="status"
					type="checkbox" 
					data-toggle="toggle" 
					data-onstyle="primary" 
					data-on="Ya" 
					data-off="Tidak">
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
		</div>
	</form>

</div>

@endsection

@push('styles')
	<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endpush
@push('scripts')
	<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
@endpush