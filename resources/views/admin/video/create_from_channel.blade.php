@extends('layouts.admin.default')

@section('title', 'Create Video')
@section('pageTitle', 'Create Video')

@section('content')

@if(session('message'))
	<div class="alert alert-custom alert-danger fade show" role="alert">
	    <div class="alert-icon"><i class="flaticon-warning"></i></div>
	    <div class="alert-text">{{ session('message') }}</div>
	    <div class="alert-close">
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	            <span aria-hidden="true"><i class="ki ki-close"></i></span>
	        </button>
	    </div>
	</div>
@endif

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Create a New Video</h3>
	</div>

	<form 
		action="{{ route('admin.video.video-from-channel') }}" 
		method="POST"
		enctype="multipart/form-data">

		@csrf

		<div class="card-body">
			<div class="form-group">
				<label>Video For 
				<span class="text-danger">*</span></label>
				<select 
					name="video_for"
					class="form-control select2 @error('video_for') is-invalid @enderror"
					id="videoFor">
					@foreach($users as $user)
						<option></option>
			      		<option 
			      			value="{{ $user->id }}"
			      			@if(collect(old('video_for'))->contains($user->id))
			      				selected="selected" 
			      			@endif
			      		>
			      		{{ $user->name }}
			      	</option>
			      	@endforeach
			     </select>
				@error('video_for')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>

			<div class="form-group">
							
				<label for="">Video Category</label>
				<select name="categories[]" id="categories" class="form-control select2 @error('categories') is-invalid @enderror" multiple="">
					@foreach($categories as $category)
						<option 
							value="{{ $category->id }}"  
							@if(collect(old('categories'))->contains($category->id))  
								selected="selected" 
							@endif
						>
							{{ $category->name }}
						</option>
					@endforeach
				</select>

				@error('categories')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>

			<div 
				class="form-group"
				id="showYoutube">
				<label>URL Youtube Channel
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="youtubeChannel" 
					class="form-control @error('youtubeChannel') is-invalid @enderror"
					value="{{old('youtubeChannel')}}" 
					placeholder="Enter a new user. " />
				<span class="form-text text-muted">
					Import videos from YouTube Channel, copy / Paste your link here.
				</span>
				@error('youtubeChannel')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			
			<div class="form-group">
				<label>Privacy Setting 
				<span class="text-danger">*</span></label>
				<select 
					name="privacy_setting"
					class="form-control @error('privacy_setting') is-invalid @enderror">
					<option value="">--Pilih Privacy Setting--</option>
					<option value="public">Public</option>
					<option value="private">Private</option>
			     </select>
				@error('privacy_setting')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
		</div>
	</form>
</div>

@endsection

@push('scripts')
	<script>
		$(function() {
			$('#videoFor').select2({
				placeholder: "Pilih Pemilik Video",
				allowClear: true,
			});

			$('#categories').select2({
				placeholder: "Pilih Kategori Video",
				allowClear: true,
			});
		});
	</script>
@endpush
