<table class="table table-bordered">
	<tr>
		<th width="30%">Title</th>
		<td>{{ $video->title }}</td>
	</tr>
	<tr>
		<th width="30%">Thumbnail</th>
		<td>
			@if(!empty($video->thumbnail))
				@if($video->thumbnail && file_exists(storage_path('app/public/video_thumbnail/'.$video->thumbnail)))
					<img src="{{$video->getImage()}}" height="90" width="120">
				@elseif($video->thumbnail)
					<img src="{{$video->getImage()}}" height="90" width="120">
				@else
					<img src="https://via.placeholder.com/96x96" width="96px">
				@endif
			@else
				<img src="https://via.placeholder.com/180x320" height="90" width="120">
			@endif
		</td>
	</tr>
	<tr>
		<th width="30%">Kategori</th>
		<td>
			@if($video->categories->isNotEmpty())
				@foreach($video->categories as $categories)
					<span class="label label-inline label-light-primary font-weight-bold">
	                    {{ $categories->name }}
	                </span>
				@endforeach
			@else
			-
			@endif
		</td>
	</tr>
	<tr>
		<th width="30%">Tags</th>
		<td>
			@foreach($video->tagVideos as $tags)
				{{$tags->tag}}
			@endforeach
		</td>
	</tr>
	<tr>
		<th width="30%">Video Type</th>
		<td>
			@if( $video->privacy_setting === 'youtube')
				Youtube
			@else
				Upload
			@endif
		</td>
	</tr>
	<tr>
		<th width="30%">About</th>
		<td>
		@if($video->about === null )
			-
		@else
			{{ $video->about }}
		@endif
		</td>
	</tr>
	<tr>
		<th width="30%">Privacy Setting</th>
		<td>
			@if( $video->privacy_setting === 'public')
				Public
			@else
				Private
			@endif
		</td>
	</tr>
	<tr>
		<th width="30%">Views</th>
		<td>
			{{ $video->views_count }} <i class="fa fa-eye"></i>
		</td>
	</tr>
	<tr>
		<th width="30%">Likes</th>
		<td>
			{{ $video->countLike() }} <i class="far fa-thumbs-up"></i>
		</td>
	</tr>
	<tr>
		<th width="30%">Dislikes</th>
		<td>
			{{ $video->countDislike() }} <i class="far fa-thumbs-down"></i>
		</td>
	</tr>
	<tr>
		<th width="30%">Uploaded at</th>
		<td>{{ $video->created_at->diffForhumans()}}</td>
	</tr>
	<tr>
		<th width="30%">Owner</th>
		<td>
			<table class="table table-bordered w-100">
				<tr>
					<th>Nama</th>
					<th>Email</th>
				</tr>
				<tr>
					<td>{{ $video->createBy->name }}</td>
					<td>{{ $video->createBy->email }}</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<th width="30%">Comments</th>
		@if($video->videoCommentParents->count())
			<td>
				<div class="accordion" id="accordionExample">
				  <div class="card">
				    <div class="card-header" id="headingOne">
				      <h2 class="mb-0">
				        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				          Show Comments
				        </button>
				      </h2>
				    </div>

				    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
				      <div class="card-body">
				       	<table class="table table-bordered w-100">
							@php
								$rowspan = 0;
							@endphp

						 	@foreach($video->videoCommentParents as $comment)
						 		@php
						 			$rowspan = $comment->parents->count();
						 			$rowspan+=1;
						 		@endphp

								<tr> 
									<th width="75%" colspan="3">{{ $comment->content}} </th> 
									<th width="25%" rowspan="{{ $rowspan }}">
										<a 
											href="{{route('admin.comments.edit', $comment->id)}}" 
											class="btn btn-warning"
											title="edit">
											<i class="fa fa-edit"></i>
										</a>

										<form 
											class="d-inline" 
											action="{{route('admin.comments.destroy', $comment->id)}}" 
											method="POST">

											@csrf
											@method('delete')

											<button 
												class="btn btn-danger"
												title="delete">
												<i class="fa fa-trash"></i>
											</button>
										</form>
									</th> 
								</tr>
								@foreach( $comment->parents as $reply) 
									<tr> 
										<td width="50%" colspan="2">{{ $reply->content }}</td> 
										<td>
											<a 
												href="{{route('admin.comments.edit', $reply->id)}}" 
												class="btn btn-warning"
												title="edit">
												<i class="fa fa-edit"></i>
											</a>

											<form 
												class="d-inline" 
												action="{{route('admin.comments.destroy', $reply->id)}}" 
												method="POST">

												@csrf
												@method('delete')

												<button 
													class="btn btn-danger"
													title="delete">
													<i class="fa fa-trash"></i>
												</button>
											</form>
										</td>  
									</tr>
								@endforeach
							@endforeach
						</table>
				      </div>
				    </div>
				  </div>
				</div>
			</td>
		@else
			<td> - </td>
		@endif
	</tr>
</table>