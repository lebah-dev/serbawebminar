@extends('layouts.admin.default')

@section('title', 'Create Video')
@section('pageTitle', 'Create Video')

@section('content')

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Create a New Video</h3>
	</div>

	<form 
		action="{{ route('admin.video.store') }}" 
		method="POST"
		enctype="multipart/form-data">

		@csrf

		<div class="card-body">
			<div class="form-group">
				<label>Video For 
				<span class="text-danger">*</span></label>
				<select 
					name="video_for"
					class="form-control select2 @error('video_for') is-invalid @enderror"
					id="videoFor">
					@foreach($users as $user)
						<option></option>
			      		<option 
			      			value="{{ $user->id }}"
			      			@if(collect(old('video_for'))->contains($user->id))
			      				selected="selected" 
			      			@endif
			      		>
			      		{{ $user->name }}
			      	</option>
			      	@endforeach
			     </select>
				@error('video_for')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>

			<div class="form-group">
							
				<label for="">Video Category</label>
				<select name="categories[]" id="categories" class="form-control select2 @error('categories') is-invalid @enderror" multiple="">
					@foreach($categories as $category)
						<option 
							value="{{ $category->id }}"  
							@if(collect(old('categories'))->contains($category->id))  
								selected="selected" 
							@endif
						>
							{{ $category->name }}
						</option>
					@endforeach
				</select>

				@error('categories')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>

			<div class="form-group">
				<label>Type Video
				<span class="text-danger">*</span></label>
				<select 
					name="video_type"
					class="form-control @error('video_type') is-invalid @enderror" 
					id="videoType">
					<option 
						id="pilih" 
						value="">
						--Pilih Type video--
					</option>
					<option 
						id="youtube" 
						value="youtube" 
						@if (old('video_type') == "youtube") {{ 'selected' }} @endif>
						Youtube
					</option>
					<option 
						id="upload" 
						value="upload" 
						@if (old('video_type') == "upload") {{ 'selected' }} @endif>
						Upload
					</option>
			     </select>
				@error('video_type')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div 
				class="form-group"
				id="showYoutube">
				<label>URL Video 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="pathYoutube"
					id="pathYoutube" 
					class="form-control @error('pathYoutube') is-invalid @enderror"
					value="{{old('path')}}" 
					placeholder="Enter a new user. " />
				<span class="form-text text-muted">
					Import videos from YouTube, copy / Paste your video link here.
				</span>
				@error('pathYoutube')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>

			<div 
				class="form-group"
				id="showTitle">
				<label>Title 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="title"
					id="title" 
					class="form-control @error('title') is-invalid @enderror"
					value="{{old('title')}}" 
					placeholder="Enter a new user. " />
				@error('title')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div 
				class="form-group"
				id="showUpload">
				<label>
					File Video
					<span class="text-danger">*</span>
				</label>
				<input 
					type="file"
					name="pathUpload"
					id="pathUpload" 
					class="form-control @error('pathUpload') is-invalid @enderror" 
					placeholder="Enter a new avatar." />
				<span class="form-text text-muted">
					Select video files to upload, select your video files.
				</span>
				@error('pathUpload')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>

			<div 
				class="form-group"
				id="showThumbnail">
				<label>Thumbnail
				<input 
					type="file"
					name="thumbnail"
					id="thumbnail" 
					class="form-control @error('thumbnail') is-invalid @enderror" 
					placeholder="Enter a new avatar." />
				<span class="form-text text-muted">
					Jika thumbnail tidak ingin ditambahkan, kolom dapat dikosongkan.
				</span>
				<span class="form-text text-muted">
					Allowed File Extensions: .jpg, .jpeg, .png
				</span>
				<span class="form-text text-muted">
						For the best results on all devices, use an image that’s at least 480 x 360 pixels and 2MB or less.
					</span>
				@error('thumbnail')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div 
				class="form-group"
				id="showAbout">
				<label>About 
				<span class="text-danger">*</span></label>
				<textarea 
					class="form-control @error('about') is-invalid @enderror"
					rows="10" 
					name="about"
					id="about">{{old('about')}}</textarea>
				@error('about')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			
			<div class="form-group">
				<label>Privacy Setting 
				<span class="text-danger">*</span></label>
				<select 
					name="privacy_setting"
					class="form-control @error('privacy_setting') is-invalid @enderror">
					<option value="">--Pilih Privacy Setting--</option>
					<option value="public">Public</option>
					<option value="private">Private</option>
			     </select>
				@error('privacy_setting')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
		</div>
	</form>

</div>

@endsection

@push('styles')
	<style>
		#showYoutube, #showUpload {
		     display: none;
		}
	</style>
@endpush

@push('scripts')
	<script>
		$(function() {
			$('#categories').select2({
				placeholder: "Pilih Kategori Video",
				allowClear: true,
			});

			$('#videoFor').select2({
				placeholder: "Pilih Pemilik Video",
				allowClear: true,
			});

			var e = document.getElementById("videoType");
			var selected = e.options[e.selectedIndex].value;

			if (selected === 'youtube') {
				$("#showYoutube").show();
				$("#pathYoutube").prop('disabled', false);

				$("#showTitle").hide();
				$("#showUpload").hide();
				$("#showThumbnail").hide();
				$("#showAbout").hide();
				$("#title").prop('disabled', true);
				$("#pathUpload").prop('disabled', true);
				$("#thumbnail").prop('disabled', true);
				$("#about").prop('disabled', true);

			} else if (selected === 'upload') {
				$("#showYoutube").hide();
				$("#pathYoutube").prop('disabled', true);

				$("#showTitle").show();
				$("#showUpload").show();
				$("#showThumbnail").show();
				$("#showAbout").show();
				$("#title").prop('disabled', false);
				$("#pathUpload").prop('disabled', false);
				$("#thumbnail").prop('disabled', false);
				$("#about").prop('disabled', false);

			} else {
				$("#showYoutube").hide();
				$("#pathYoutube").prop('disabled', true);
				
				$("#showTitle").hide();
				$("#showUpload").hide();
				$("#showThumbnail").hide();
				$("#showAbout").hide();
				$("#title").prop('disabled', true);
				$("#pathUpload").prop('disabled', true);
				$("#thumbnail").prop('disabled', false);
				$("#about").prop('disabled', true);
			}
			

			$("#videoType").on("change", function() {
				var valueInput = $(this).val();
				
				if (valueInput === 'youtube') {
					$("#showYoutube").show();
					$("#pathYoutube").prop('disabled', false);

					$("#showTitle").hide();
					$("#showUpload").hide();
					$("#showThumbnail").hide();
					$("#showAbout").hide();
					$("#title").prop('disabled', true);
					$("#pathUpload").prop('disabled', true);
					$("#thumbnail").prop('disabled', true);
					$("#about").prop('disabled', true);

				} else if (valueInput === 'upload') {
					$("#showYoutube").hide();
					$("#pathYoutube").prop('disabled', true);

					$("#showTitle").show();
					$("#showUpload").show();
					$("#showThumbnail").show();
					$("#showAbout").show();
					$("#title").prop('disabled', false);
					$("#pathUpload").prop('disabled', false);
					$("#thumbnail").prop('disabled', false);
					$("#about").prop('disabled', false);

				} else {
					$("#showYoutube").hide();
					$("#pathYoutube").prop('disabled', true);
					
					$("#showTitle").hide();
					$("#showUpload").hide();
					$("#showThumbnail").hide();
					$("#showAbout").hide();
					$("#title").prop('disabled', true);
					$("#pathUpload").prop('disabled', true);
					$("#thumbnail").prop('disabled', false);
					$("#about").prop('disabled', true);
					
				}
			
			});
		});
	</script>
@endpush