@extends('layouts.admin.default')

@section('title', 'Search Video from Youtube Channel')
@section('pageTitle', 'Search Video from Youtube Channel')

@section('content')

@error('videoId')
	<div class="alert alert-custom alert-danger fade show" role="alert">
	    <div class="alert-icon"><i class="flaticon-warning"></i></div>
	    <div class="alert-text">{{ $message }}</div>
	    <div class="alert-close">
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	            <span aria-hidden="true"><i class="ki ki-close"></i></span>
	        </button>
	    </div>
	</div>
@enderror

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header flex-wrap">
		<div class="card-title">
			<h3 class="card-label">List Search Video from </h3>
		</div>
		 <div class="card-toolbar">
	        <form 
	        	action="{{ route('admin.video.search.video-from-channel')}}"
	        	method="POST">
	        	@csrf
				<div class="input-group" style="width: 374px;">
					<input 
						type="text" 
						name="keyword"
						class="form-control @error('keyword') is-invalid @enderror"
						placeholder="Filter berdasarkan title"
						value="{{old('keyword')}}" >
						<input type="" value="{{ $my_videos['channelId'] }}" name="channelId" hidden>

					<div class="input-group-append">
						<input 
							type="submit" 
							value="Filter"
							class="btn btn-primary">
					</div>
					@error('keyword')
						<span class="invalid-feedback">
							{{ $message }}
						</span>
					@enderror
				</div>
			</form>
	    </div>
	</div>
	
	<div class="card-body">
		<form 
			action="{{ route('admin.video.store-video-from-channel') }}" 
			method="POST"
			enctype="multipart/form-data">

			@csrf
			<span>ID Channel : <strong>{{ $my_videos['channelId'] }}</strong></span>
			<br>
			<br>
			<button type="submit" class="btn btn-primary mb-2 upload_all" data-url="#">Upload All Selected</button>
			<table 
				class="table table-bordered table-hover table-checkable mt-10" 
				id="videosTable">
				<thead>
					<tr>
						<th width="5%"><input type="checkbox" id="master"></th>
						<th width="5%">No.</th>
						<th width="65%">Title</th>
						<th width="25%">Thumbnail</th>
					</tr>
				</thead>
				<tbody>
					@if(session('search_video_from_channel') != null)
						@forelse(session('search_video_from_channel')['items'] as $key => $videos)
							<tr id="tr_{{ $key }}">
								<td><input type="checkbox" class="sub_chk" name="videoId[]" value="{{ $videos['v_id'] }}"></td>
								<td>{{ $key +=1 }}</td>
								<td>{{ $videos['v_title'] }}</td>
								<td>
									<img src="{{ $videos['v_thumbnail'] }}" height="90" width="120">
								</td>
							</tr>
						@empty
							<tr>
								<td colspan="5"  style="text-align: center; vertical-align: middle;">
									Video tidak ditemukan
								</td>
							</tr>
						@endforelse
					@else
						<tr>
							<td colspan="5"  style="text-align: center; vertical-align: middle;">
								Video tidak ditemukan
							</td>
						</tr>
					@endif
				</tbody>
			</form>
				@if($search_videos != null && $search_videos['nextPageToken'] != null)
					<tfoot>
						<tr>
							<td colspan="5"  style="text-align: center; vertical-align: middle;">
								<a 
									href="{{ route('admin.video.search.more-video-from-channel')}}"
									type="button" 
									class="btn btn-primary" 
									onclick="event.preventDefault(); document.getElementById('load-more').submit();">
									Load More
								</a>
								<form 
									id="load-more" 
									action="{{ route('admin.video.search.more-video-from-channel') }}" 
									method="POST" 
									style="display: none;">
						            @csrf
						            
						            <input type="" value="{{ $search_videos['channelId'] }}" name="channelId" hidden>
						            <input type="" value="{{ $search_videos['nextPageToken'] }}" name="nextTokenPage" hidden="">
							        
					          </form>
							</td>
						</tr>
					</tfoot>
				@endif
			</table>
	</div>
	

</div>
@endsection

@push('scripts')
	<script>
		$(function() {
			$('#videoFor').select2({
				placeholder: "Pilih Pemilik Video",
				allowClear: true,
			});

			$('#master').on('click', function(e) {
		         if($(this).is(':checked',true))  
		         {
		            $(".sub_chk").prop('checked', true);  
		         } else {  
		            $(".sub_chk").prop('checked',false);  
		         }  
	        });
		});
	</script>
@endpush