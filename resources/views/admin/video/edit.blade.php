@extends('layouts.admin.default')

@section('title', 'Edit Video')
@section('pageTitle', 'Edit Video')

@section('content')

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Edit Video</h3>
	</div>

	<form 
		action="{{ route('admin.video.update', $video->id) }}" 
		method="POST"
		enctype="multipart/form-data">

		@csrf
		@method('PUT')

		<div class="card-body">
			<div class="form-group">
				<label>Video Owner</label>
				<input 
					type="text"
					name="videoOwner" 
					class="form-control"
					value="{{ $video->createBy->name }}" 
					placeholder="Enter a new user. " 
					readonly="" />
			</div>

			<div class="form-group">
							
				<label for="">Video Category</label>
				<select name="categories[]" id="categories" class="form-control select2 @error('categories') is-invalid @enderror" multiple="">
					@foreach($categories as $category)
						<option 
							value="{{ $category->id }}"  
							@if($video->categories->contains($category)) 
								selected="selected" 
							@endif
							@if(collect(old('categories'))->contains($category->id)) 
								selected="selected" 
							@endif
						>
							{{ $category->name }}
						</option>
					@endforeach
				</select>

				@error('categories')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>

			<div class="form-group">
				<label>Title 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="title" 
					class="form-control @error('title') is-invalid @enderror"
					value="{{ old('title') ?? $video->title }}" 
					placeholder="Enter a new user. " 
					@if($video->video_type === 'youtube') readonly="" @endif/>
				@error('title')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Type Video</label>
				<input 
					type="text"
					name="video_type" 
					class="form-control"
					value="{{ ucwords($video->video_type) }}" 
					placeholder="Enter a new user. " 
					readonly="" />
			</div>

			@if($video->video_type === 'youtube')
			<div class="form-group">
				<label>Embed Video 
				<span class="text-danger">*</span></label>
				<br>
				<iframe 
					width="320" 
					height="180"
					src="https://www.youtube.com/embed/{{ $video->path }}">
				</iframe>
				<br><br>
				<input 
					type="text"
					name="pathYoutube" 
					class="form-control @error('pathYoutube') is-invalid @enderror"
					value="{{old('path') }}" 
					placeholder="Enter a URL youtube. " />
				<span class="form-text text-muted">
					Jika video tidak ingin dirubah, kolom dapat dikosongkan.
				</span>
				<span class="form-text text-muted">
					Import videos from YouTube, copy / Paste your video link here.
				</span>
				@error('pathYoutube')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			@endif

			@if($video->video_type === 'upload')
			<div class="form-group">
				<label>
					File Video
					<span class="text-danger">*</span>
				</label>
				<br>
				<video 
					width="320" 
					height="180" 
					controls>
				  <source src="{{ $video->getVideo() }}" type="video/mp4">
				</video>
				<br><br>
				<input 
					type="file"
					name="pathUpload" 
					class="form-control @error('pathUpload') is-invalid @enderror" 
					placeholder="Enter a new avatar." />
				<span class="form-text text-muted">
					Jika File Video tidak ingin dirubah, kolom dapat dikosongkan.
				</span>
				<span class="form-text text-muted">
					Select video files to upload, select your video files.
				</span>
				@error('pathUpload')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			@endif

			<div class="form-group">
				<label>Thumbnail</label><br>
				<small class="text-muted">Current image</small><br>
				@if(!empty($video->thumbnail))
					@if($video->thumbnail && file_exists(storage_path('app/public/video_thumbnail/'.$video->thumbnail)))
						<img src="{{$video->getImage()}}" height="180" width="320">
					@elseif($video->thumbnail)
						<img src="{{$video->getImage()}}" height="180" width="320">
					@else
						<img src="https://via.placeholder.com/96x96" width="96px">
					@endif
				@else
					<img src="https://via.placeholder.com/180x320" height="180" width="320">
				@endif
				<br><br>
				@if($video->video_type === 'upload') 
					<input 
						type="file"
						name="thumbnail" 
						class="form-control @error('thumbnail') is-invalid @enderror" 
						placeholder="Enter a new avatar."/>
					<span class="form-text text-muted">
						Jika thumbnail tidak ingin dirubah, kolom dapat dikosongkan.
					</span>
					<span class="form-text text-muted">
						Allowed File Extensions: .jpg, .jpeg, .png
					</span>
					<span class="form-text text-muted">
						For the best results on all devices, use an image that’s at least 480 x 360 pixels and 2MB or less.
					</span>
					@error('thumbnail')
						<span class="invalid-feedback">
							{{ $message }}
						</span>
					@enderror
				@endif
			</div>
			<div class="form-group">
				<label>About 
				<span class="text-danger">*</span></label>
				<textarea 
					class="form-control @error('about') is-invalid @enderror"
					rows="10"
					@if($video->video_type === 'youtube') readonly="" @endif 
					name="about">{{old('about') ?? $video->about }}</textarea>
				@error('about')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>

			<div class="form-group">
				<label>Privacy Setting 
				<span class="text-danger">*</span></label>
				<select 
					name="privacy_setting"
					class="form-control @error('privacy_setting') is-invalid @enderror">
					<option 
						value=""
						@if ((old('privacy_setting') == " ") || ($video->privacy_setting == '')) {{ 'selected' }} @endif>
						--Pilih Privacy Setting--
					</option>
					<option 
						value="public"
						@if ((old('privacy_setting') == "public") || ($video->privacy_setting == 'public')) {{ 'selected' }} @endif>
						Public
					</option>
					<option 
						value="private"
						@if ((old('privacy_setting') == "private") || ($video->privacy_setting == 'private')) {{ 'selected' }} @endif>
						Private
					</option>
			     </select>
				@error('privacy_setting')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
		</div>
	</form>

</div>

@endsection

@push('scripts')
<script>
	$(function(){
		$('#categories').select2({
			placeholder: "Pilih Kategori Video",
			allowClear: true,
		});
	})
</script>
@endpush
