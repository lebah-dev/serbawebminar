@extends('layouts.admin.default')

@section('title', 'Create Admin')
@section('pageTitle', 'Create Admin')

@section('content')

@if(session('error'))
    <div class="alert alert-custom alert-danger fade show" role="alert">
        <div class="alert-icon"><i class="flaticon-warning"></i></div>
        <div class="alert-text">{{ session('error') }}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
@endif

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Create a New Admin</h3>
	</div>

	<form 
		action="{{ route('admin.admins.store') }}" 
		method="POST"
		enctype="multipart/form-data">

		@csrf

		<div class="card-body">
			<div class="form-group">
				<label>Name 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="name" 
					class="form-control @error('name') is-invalid @enderror"
					value="{{old('name')}}" 
					placeholder="Enter a new admin. " />
				@error('name')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Email 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="email" 
					class="form-control @error('email') is-invalid @enderror"
					value="{{old('email')}}" 
					placeholder="Enter a new email. " />
				@error('email')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Avatar
				<input 
					type="file"
					name="avatar" 
					class="form-control @error('avatar') is-invalid @enderror" 
					placeholder="Enter a new avatar." />
				<span class="form-text text-muted">
					Jika tidak ingin menambahkan avatar, kolom dapat dikosongkan.
				</span>
				<span class="form-text text-muted">
					Allowed File Extensions: .jpg, .jpeg, .png
				</span>
				@error('avatar')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Password 
				<span class="text-danger">*</span></label>
				<input 
					type="password"
					name="password" 
					class="form-control @error('password') is-invalid @enderror"
					value="" 
					placeholder="Enter a new user. " />
				@error('password')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Konfirmasi Password 
				<span class="text-danger">*</span></label>
				<input 
					type="password"
					name="password_confirmation" 
					class="form-control @error('password_confirmation') is-invalid @enderror"
					value="" 
					placeholder="Enter a new user. " />
				@error('password_confirmation')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
		</div>
	</form>

</div>

@endsection