@extends('layouts.admin.default')

@section('title', 'Admins')
@section('pageTitle', 'Admins')

@section('content')

  @if(session('success'))
    <div class="alert alert-custom alert-primary fade show" role="alert">
        <div class="alert-icon"><i class="flaticon-warning"></i></div>
        <div class="alert-text">{{ session('success') }}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
  @endif

  <div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
      <div class="card-title">
        <h3 class="card-label">Data Admins 
      </div>
      <div class="card-toolbar">
        <a 
          href = "{{ route('admin.admins.create')}}"
          class="btn btn-primary font-weight-bolder">

          <span class="svg-icon svg-icon-md">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <rect x="0" y="0" width="24" height="24" />
                <circle fill="#000000" cx="9" cy="15" r="6" />
                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
              </g>
            </svg>
          </span>
          New Record
        </a>
      </div>
    </div>
    <div class="card-body">
      <table 
        class="table table-bordered table-hover table-checkable mt-10" 
        id="adminsTable">
        <thead>
          <tr>
            <th width="5%">No.</th>
            <th width="75%">Name</th>
            <th width="30%">Actions</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
@endsection

@push('styles')
  <link href="{{asset('assets/admin/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endpush

@push('scripts')
  <script src="{{asset('assets/admin/plugins/custom/datatables/datatables.bundle.js')}}"></script>
  <script src="{{asset('assets/admin/js/pages/crud/datatables/basic/headers.js')}}"></script>
  <script src="{{asset('assets/admin/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

  <script>
  $(function(){
    $('#adminsTable').DataTable({
        "processing" : true,
        "serverSide" : true,
        "responsive" : true,
        "autoWidth"  : true,
      ajax: '{{ route('admin.admins.data') }}',
      columns: [
          {data: 'DT_RowIndex', orderable: false, searchable: false},
          {data: 'name'},
          {data: 'action'}  
      ]
    });

    $('#adminsTable'). on('click', 'button#delete', function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                
                Swal.fire({
                title: 'Kamu yakin hapus data ini?',
                text: "data tidak bisa dikembalikan",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oke, hapus!',
                cancelButtonText: 'Batalkan'
                }).then((result) => {
                    if (result.value) {
                        
                        $.ajax({
                            type: "DELETE",
                            url: "admins/" + id,
                            data: {
                                "id" : id,
                                "_token" : "{{ csrf_token() }}"
                            },
                            success: function(data) {
                                Swal.fire(
                                    'Berhasil dihapus!',
                                    'Data kamu telah dihapus',
                                    'success'
                                )
                                location.reload(true);
                            }
                        })
                    }
                })
            });
  });

  </script>
@endpush