<table class="table table-bordered">
	<tr>
		<th>Name</th>
		<td>{{ $admin->name }}</td>
	</tr>
	<tr>
		<th>Email</th>
		<td>{{ $admin->email}}</td>
	</tr>
	<tr>
		<th>Joined</th>
		<td>{{ $admin->created_at->diffForhumans()}}</td>
	</tr>
</table>