@extends('layouts.admin.default')

@section('title', 'Create Admin')
@section('pageTitle', 'Edit Admin')

@section('content')

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Edit Admin</h3>
	</div>

	<form 
		action="{{ route('admin.admins.update', $admin->id) }}" 
		method="POST"
		enctype="multipart/form-data">

		@csrf
		@method('PUT')

		<div class="card-body">
			<div class="form-group">
				<label>Name 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="name" 
					class="form-control @error('name') is-invalid @enderror"
					value="{{old('name') ?? $admin->name}}" 
					placeholder="Enter a new user. " />
				@error('name')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Email 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="email" 
					class="form-control @error('email') is-invalid @enderror"
					value="{{old('email') ?? $admin->email }}" 
					placeholder="Enter a new email. " />
				@error('email')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Avatar</label><br>
				<small class="text-muted">Current image</small><br>
				@if(!empty($admin->admin_avatar->thumbnail))
					@if( $admin->admin_avatar->thumbnail && Storage::exists($admin->admin_avatar->thumbnail) )
						<img src="{{$admin->admin_avatar->getImage()}}" height="100" width="100">
					@else
						<img src="https://via.placeholder.com/96x96" width="96px">
					@endif
				@else
					<img src="https://via.placeholder.com/96x96" width="96px">
				@endif
				<br><br>

				<input 
					type="file"
					name="avatar" 
					class="form-control @error('avatar') is-invalid @enderror" 
					placeholder="Enter a new avatar." />
				<span class="form-text text-muted">
					Jika avatar tidak ingin dirubah, kolom dapat dikosongkan.
				</span>
				<span class="form-text text-muted">
					Allowed File Extensions: .jpg, .jpeg, .png
				</span>
				@error('avatar')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Password 
				<span class="text-danger">*</span></label>
				<input 
					type="password"
					name="password" 
					class="form-control @error('password') is-invalid @enderror"
					value="" 
					placeholder="Enter a new user. " />
				@error('password')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Konfirmasi Password 
				<span class="text-danger">*</span></label>
				<input 
					type="password"
					name="password_confirmation" 
					class="form-control @error('password_confirmation') is-invalid @enderror"
					value="" 
					placeholder="Enter a new user. " />
				@error('password_confirmation')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
		</div>
	</form>

</div>

@endsection