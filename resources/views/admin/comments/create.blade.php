@extends('layouts.admin.default')

@section('title', 'Create Comment')
@section('pageTitle', 'Create Comment')

@section('content')

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Create a New Comment</h3>
	</div>

	<form 
		action="{{ route('admin.comments.store') }}" 
		method="POST"
		enctype="multipart/form-data">

		@csrf

		<div class="card-body">
			<div class="form-group">
				<label>From User 
				<span class="text-danger">*</span></label>
				<select 
					name="user_id"
					class="form-control select2 @error('user_id') is-invalid @enderror"
					id="fromUser">
					@foreach($users as $user)
						<option></option>
			      		<option 
			      			value="{{ $user->id }}"
			      			@if(collect(old('user_id'))->contains($user->id))
			      				selected="selected" 
			      			@endif
			      		>
			      		{{ $user->name }}
			      	</option>
			      	@endforeach
			     </select>
				@error('user_id')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>For Video 
				<span class="text-danger">*</span></label>
				<select 
					name="video_id"
					class="form-control select2 @error('video_id') is-invalid @enderror"
					id="forVideo">
					@foreach($videos as $video)
						<option></option>
			      		<option 
			      			value="{{ $video->id }}"
			      			@if(collect(old('video_id'))->contains($video->id))
			      				selected="selected" 
			      			@endif
			      		>
			      		{{ $video->title }}
			      	</option>
			      	@endforeach
			     </select>
				@error('video_id')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Comment 
				<span class="text-danger">*</span></label>
				<textarea 
					class="form-control @error('content') is-invalid @enderror"
					rows="10" 
					name="content">{{old('about')}}</textarea>
				@error('content')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
		</div>
	</form>

</div>

@endsection

@push('scripts')
	<script>
		$(function() {
			$('#fromUser').select2({
				placeholder: "Pilih Pengirim Komentar",
				allowClear: true,
			});
			$('#forVideo').select2({
				placeholder: "Pilih Video dikomentari",
				allowClear: true,
			});
		});
	</script>
@endpush
