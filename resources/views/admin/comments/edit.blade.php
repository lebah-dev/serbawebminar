@extends('layouts.admin.default')

@section('title', 'Edit Comment')
@section('pageTitle', 'Edit Comment')

@section('content')

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Edit Comment</h3>
	</div>

	<form 
		action="{{ route('admin.comments.update', $comment->id) }}" 
		method="POST"
		enctype="multipart/form-data">

		@csrf
		@method('PUT')

		<div class="card-body">
			<div class="form-group">
				<label>From User 
				<span class="text-danger">*</span></label>
				<select 
					name="user_id"
					class="form-control select2 @error('user_id') is-invalid @enderror"
					id="fromUser"
					readonly="readonly"
					disabled="">
			      		<option 
			      			value="{{ $comment->user_id }}"
			      			selected="selected">
			      		{{ $comment->user->name }}
			      	</option>
			     </select>
				@error('user_id')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>For Video 
				<span class="text-danger">*</span></label>
				<select 
					name="video_id"
					class="form-control select2 @error('video_id') is-invalid @enderror"
					id="forVideo"
					readonly="readonly"
					disabled="">
					<option 
			      			value="{{ $comment->video_id }}"
			      			selected="selected">
			      		{{ $comment->video->title }}
			      	</option>
			     </select>
				@error('video_id')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Comment 
				<span class="text-danger">*</span></label>
				<textarea 
					class="form-control @error('content') is-invalid @enderror"
					rows="10" 
					name="content">{{old('about') ?? $comment->content}}</textarea>
				@error('content')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
		</div>
	</form>

</div>

@endsection

@push('scripts')
	<script>
		$(function() {
			$('#fromUser').select2({
				placeholder: "Pilih Pengirim Komentar",
				allowClear: true,
			});
			$('#forVideo').select2({
				placeholder: "Pilih Video dikomentari",
				allowClear: true,
			});
		});
	</script>
@endpush
