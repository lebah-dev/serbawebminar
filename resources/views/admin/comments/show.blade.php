<table class="table table-bordered">

	<tr>
		<th>From</th>
		<td>{{ $comment->user->name }}</td>
	</tr>
	<tr>
		<th>For Video</th>
		<td>{{ $comment->video->title}}</td>
	</tr>
	<tr>
		<th>Comment</th>
		<td>{{ $comment->content }}</td>
	</tr>
	<tr>
		<th>Created at</th>
		<td>{{ $comment->created_at->diffForhumans() }}</td>
	</tr>
	<tr>
		<th>Total Like</th>
		<td>
			{{ $comment->likes_count}}
		</td>
	</tr>
	<tr>
		<th>Total Dislike</th>
		<td>
			{{ $comment->dislikes_count}}
		</td>
	</tr>
	<tr>
		<th>Replies</th>
		@if($comment->parents->count() > 0 )
			<td>
				<table class="table table-bordered w-100">
					<tr>
						<th>From</th>
						<th>Reply</th>
						<th>Created at</th>
						<th>Action</th>
					</tr>
					@foreach($comment->parents as $reply)
					<tr>
						<td>{{ $reply->user->name}}</td>
						<td>{{ $reply->content }}</td>
						<td>{{ $reply->created_at->diffForhumans() }}</td>
						<td>
							<form 
								action="{{ route('admin.comments.destroyReply', $reply->id) }}" 
								method="POST">

								@csrf
								@method('delete')

								<button 
									class="btn btn-danger"
									title="delete">
									<i class="fa fa-trash"></i>
								</button>
							</form>
						</td>
					</tr>
					@endforeach
				</table>
			</td>
		@else
			<td>-</td>
		@endif
	</tr>
</table>