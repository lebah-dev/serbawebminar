@extends('layouts.admin.default')

@section('title', 'Create Channel')
@section('pageTitle', 'Create Channel')

@section('content')

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Create a New Channel</h3>
	</div>

	<form 
		action="{{ route('admin.channel.store') }}" 
		method="POST"
		enctype="multipart/form-data"
		class="form">

		@csrf

		<div class="card-body">
			<div class="form-group">
				<label>Name 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="name" 
					class="form-control @error('name') is-invalid @enderror"
					value="{{old('name')}}" 
					placeholder="Enter a new channel. " />
				@error('name')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Channel For 
				<span class="text-danger">*</span></label>
				<select 
					name="user_id"
					class="form-control @error('user_id') is-invalid @enderror" 
					id="channelFor">
					{{-- @foreach($users as $user) --}}
						{{-- <option></option> --}}
			      		<option 
			      			value="{{ $users->id }}"
			      			@if(collect(old('user_id'))->contains($users->id))
			      				selected="selected" 
			      			@endif
			      		>
			      		{{ $users->name }}
			      	</option>
			      	{{-- @endforeach --}}
			     </select>
				@error('user_id')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>

			<div class="form-group">
				<label>Business
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="business" 
					class="form-control @error('business') is-invalid @enderror"
					value="{{old('business')}}" 
					placeholder="Enter a your business. " />
				@error('business')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>

			<div class="form-group">
				<label>Image</label>
				<input 
					type="file"
					name="cover_image" 
					class="form-control @error('cover_image') is-invalid @enderror" 
					placeholder="Enter a new image." />
				<span class="form-text text-muted">
					Allowed File Extensions: .jpg, .jpeg, .png
				</span>
				@error('cover_image')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Description 
				<span class="text-danger">*</span></label>

				<textarea 
					class="form-control @error('description') is-invalid @enderror"
					rows="10" 
					name="description">{{old('description')}}</textarea>
				@error('description')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
		</div>
	</form>

</div>

@endsection

{{-- @push('scripts')
	<script>
		$(function() {
			$('#channelFor').select2({
				placeholder: "Pilih Pemilik Channel",
				allowClear: true,
			});
		});
	</script>
@endpush --}}

