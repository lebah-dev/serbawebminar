<table class="table table-bordered">
	<tr>
		<th>Name</th>
		<td>{{ $channel->name }}</td>
	</tr>
	<tr>
		<th>Description</th>
		<td>{{ $channel->description }}</td>
	</tr>
	<tr>
		<th>Cover Image</th>
		<td>
			@if( $channel->cover_image && file_exists(storage_path('app/public/channel/'.$channel->cover_image)))
				<img src="{{$channel->getImage()}}" height="100" width="100">
			@else
				<img src="https://via.placeholder.com/96x96" width="96px">
			@endif
		</td>
	</tr>
	<tr>
		<th>Links</th>
		<td>
			@if($channel->links === null || empty($channel->links))
				-
			@else
				{{ $channel->links }}
			@endif
		</td>
	</tr>
	<tr>
		<th>Business</th>
		<td>
			@if( $channel->business === null || empty($channel->business))
				-
			@else
				{{ $channel->business }}
			@endif
		</td>
	</tr>	
	<tr>
		<th>Created at</th>
		<td>{{ $channel->created_at->diffForhumans()}}</td>
	</tr>
	<tr>
		<th>Owner</th>
		<td>
			<table class="table table-bordered w-100">
				<tr>
					<th>Nama</th>
					<th>Email</th>
				</tr>
				<tr>
					<td>{{ $channel->user->name }}</td>
					<td>{{ $channel->user->email }}</td>
				</tr>
			</table>
		</td>
	</tr>
</table>