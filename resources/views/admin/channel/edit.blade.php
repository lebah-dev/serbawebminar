@extends('layouts.admin.default')

@section('title', 'Create Edit')
@section('pageTitle', 'Create Edit')

@section('content')

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Create Edit</h3>
	</div>

	<form 
		action="{{ route('admin.channel.update', $channel->id) }}" 
		method="POST"
		enctype="multipart/form-data"
		class="form">

		@csrf
		@method('PUT')

		<div class="card-body">
			<div class="form-group">
				<label>Name 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="name" 
					class="form-control @error('name') is-invalid @enderror"
					value="{{old('name') ?? $channel->name}}" 
					placeholder="Enter a new channel. " />
				@error('name')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Channel For 
				<span class="text-danger">*</span></label>
				<select 
					name="user_id"
					class="form-control @error('user_id') is-invalid @enderror"
					id="channelFor"
					readonly="readonly"
					disabled="" 
					>
			      		<option 
			      			value="{{ $channel->user_id }}"
			      			selected="selected">
			      		{{ $channel->user->name }}
			      	</option>
			     </select>
				@error('user_id')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>

			<div class="form-group">
				<label>Business
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="business" 
					class="form-control @error('business') is-invalid @enderror"
					value="{{old('business') ?? $channel->business}}" 
					placeholder="Enter a your business. " />
				@error('business')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>

			<div class="form-group">
				<label>Image</label><br>
				<small class="text-muted">Current image</small><br>
				@if(!empty($channel->cover_image))
					@if($channel->cover_image && file_exists(storage_path('app/'.$channel->cover_image)))
						<img src="{{$channel->getImage()}}" height="100" width="100">
					@else
						<img src="https://via.placeholder.com/96x96" width="96px">
					@endif
				@else
					<img src="https://via.placeholder.com/96x96" width="96px">
				@endif
				<br><br>
				<input 
					type="file"
					name="cover_image" 
					class="form-control @error('cover_image') is-invalid @enderror" 
					placeholder="Enter a new image." />
				<span class="form-text text-muted">
					Jika Image tidak ingin dirubah, kolom dapat dikosongkan.
				</span>
				<span class="form-text text-muted">
					Allowed File Extensions: .jpg, .jpeg, .png
				</span>
				@error('cover_image')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Description 
				<span class="text-danger">*</span></label>

				<textarea 
					class="form-control @error('description') is-invalid @enderror"
					rows="10" 
					name="description">{{old('description') ?? $channel->description }}</textarea>
				@error('description')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
		</div>
	</form>

</div>

@endsection

