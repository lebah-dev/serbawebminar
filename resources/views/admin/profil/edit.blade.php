@extends('layouts.admin.default')

@section('title', 'My Profile')
@section('pageTitle', 'My Profile')

@section('content')
	<div class="card card-custom">
		<div class="card-header flex-wrap border-0 pt-6 pb-0">
			<div class="card-title">
				<h3 class="card-label">Edit Profile 
			</div>
		</div>
		<div class="card-body">
			<form 
				class="form"
				action="{{ route('admin.profil.update', $admin->id )}}"
				method="POST"
				enctype="multipart/form-data">

			@csrf
			@method('PUT')

			 <div class="card-body">
			  <div class="mb-15">
			   <div class="form-group row">
			    <label class="col-lg-3 col-form-label">Full Name:</label>
			    <div class="col-lg-6">
			     <input 
			     	type="text" 
			     	class="form-control @error('name') is-invalid @enderror" 
			     	placeholder="Enter full name"/
			     	name="name"
			     	value="{{ old('name') ?? $admin->name}}">

			     	@error('name')
			     		<span class="invalid-feedback">
								{{ $message }}
							</span>
			     	@enderror
			    </div>
			   </div>
			   <div class="form-group row">
			    <label class="col-lg-3 col-form-label">Email address:</label>
			    <div class="col-lg-6">
			     <input 
			     	type="email" 
			     	class="form-control @error('email') is-invalid @enderror" 
			     	placeholder="Enter email"/
			     	name="email"
			     	value="{{ old('email') ?? $admin->email}}">

			     	@error('email')
			     		<span class="invalid-feedback">
								{{ $message }}
							</span>
			     	@enderror
			    </div>
			   </div>
			   <div class="form-group row">
			    <label class="col-lg-3 col-form-label">Photo Profile:</label>
			    <div class="col-lg-6">

					  <small class="text-muted">Current Avatar</small><br>
					  @if(!empty($admin->admin_avatar->thumbnail))
							@if($admin->admin_avatar->thumbnail && file_exists(storage_path('app/public/adminAvatar/'.$admin->admin_avatar->thumbnail)))
										<img src="{{$admin->admin_avatar->getImage()}}" height="100" width="100">
							@else
								<img src="https://via.placeholder.com/96x96" width="96px">
							@endif
						@else
							<img src="https://via.placeholder.com/96x96" width="96px">
						@endif
						<br><br>

			     <input 
			     	type="file" 
			     	class="form-control @error('avatar') is-invalid @enderror"/
			     	name="avatar">

			     	<span class="form-text text-muted">
							Jika gambar tidak ingin dirubah, kolom dapat dikosongkan.
						</span>
						<span class="form-text text-muted">
							Allowed File Extensions: .jpg, .jpeg, .png
						</span>
						@error('avatar')
							<span class="invalid-feedback">
								{{ $message }}
							</span>
						@enderror
			    
			    </div>
			   </div>
			  </div>
			 </div>
			 <div class="card-footer">
			  <div class="row">
			   <div class="col-lg-12">
			    <button type="submit" class="btn btn-success mr-2">Simpan Perubahan</button>
			   </div>
			  </div>
			 </div>
			</form>
		</div>
	</div>
			
@endsection

