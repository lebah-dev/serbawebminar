@extends('layouts.admin.default')

@section('title', 'My Profile')
@section('pageTitle', 'My Profile')

@section('content')
	  @if(session('message'))
	    <div class="alert alert-custom alert-primary fade show" role="alert">
	        <div class="alert-icon"><i class="flaticon-warning"></i></div>
	        <div class="alert-text">{{ session('message') }}</div>
	        <div class="alert-close">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                <span aria-hidden="true"><i class="ki ki-close"></i></span>
	            </button>
	        </div>
	    </div>
	  @endif

	  <div class="card card-custom">
			 <div class="card-header card-header-tabs-line">
			  <div class="card-toolbar">
			   <ul class="nav nav-tabs nav-bold nav-tabs-line">
			    <li class="nav-item">
			     <a class="nav-link @if($errors->has('password') || $errors->has('current_password') || $errors->has('password_confirmation')) '' @else show active @endif" data-toggle="tab" href="#kt_tab_pane_1_4">
			     <span class="nav-icon"><i class="flaticon2-user"></i></span>
			     <span class="nav-text">My Profile</span>
			     </a>
			    </li>
			    <li class="nav-item">
			     <a class="nav-link @if($errors->has('password') || $errors->has('current_password') || $errors->has('password_confirmation')) active @else '' @endif" data-toggle="tab" href="#kt_tab_pane_2_4">
			     <span class="nav-icon"><i class="flaticon2-lock"></i></span>
			     <span class="nav-text">Password</span>
			     </a>
			    </li>
			   </ul>
			  </div>
			 </div>
			 <div class="card-body">
			  <div class="tab-content">
			   <div class="tab-pane fade @if($errors->has('password') || $errors->has('current_password') || $errors->has('password_confirmation')) '' @else show active @endif" id="kt_tab_pane_1_4" role="tabpanel" aria-labelledby="kt_tab_pane_1_4">
			   	<form 
			   		class="form"
			   		
			   		method="POST">
			   		
			   		@csrf
			   		@method('PUT')

					 <div class="card-body">
					 	<div class="row">
					 		<div class="col-lg-9">
							  <div class="mb-15">
							   <div class="form-group row">
							    <label class="col-lg-3 col-form-label">Full Name:</label>
							    <div class="col-lg-6">
							     <span class="form-text">{{ $admin->name }}</span>
							    </div>
							   </div>
							   <div class="form-group row">
							    <label class="col-lg-3 col-form-label">Email address:</label>
							    <div class="col-lg-6">
							     <span class="form-text">{{ $admin->email}}</span>
							    </div>
							   </div>
							  </div>
						 	</div>
						 	<div class="col-lg-3">
						 		<div class="symbol symbol-100">
									{{-- <div class="symbol-label" style="background-image:url('https://via.placeholder.com/100')"></div> --}}
									<small class="text-muted">Current Avatar</small>
								  	@if(!empty($admin->admin_avatar->thumbnail))
										@if($admin->admin_avatar->thumbnail && file_exists(storage_path('app/public/adminAvatar/'.$admin->admin_avatar->thumbnail)))
									
											<div class="symbol-label" style="background-image:url('{{$admin->admin_avatar->getImage()}}')"></div>
										@else
											<div class="symbol-label" style="background-image:url('https://via.placeholder.com/100')"></div>
										@endif
									@else
										<div class="symbol-label" style="background-image:url('https://via.placeholder.com/100')"></div>
									@endif
									{{-- <i class="symbol-badge bg-success"></i> --}}
								</div>
						 	</div>
					 	</div>
					 </div>
					 <div class="card-footer">
					  <div class="row">
					   <div class="col-lg-12">
					    <a href="{{ route('admin.profil.edit') }}"type="button" class="btn btn-success mr-2">Edit Profil</a>
					   </div>
					  </div>
					 </div>
					</form>
			   </div>
			   <div class="tab-pane fade @if($errors->has('password') || $errors->has('current_password') || $errors->has('password_confirmation')) show active @else '' @endif" id="kt_tab_pane_2_4" role="tabpanel" aria-labelledby="kt_tab_pane_2_4">
			    <form 
			    	class="form"
			    	action="{{ route('admin.profil.changePassword', $admin->id)}}"
			    	method="POST">

			    	@csrf
			    	@method('PUT')

					 <div class="card-body">
					  <div class="mb-15">
					   <div class="form-group row">
					    <label class="col-lg-3 col-form-label">Password Saat ini:</label>
					    <div class="col-lg-6">
					     <input 
					     	type="password" 
					     	class="form-control @error('current_password') is-invalid @enderror" 
					     	placeholder="Masukkan Password Saat ini"
					     	name="current_password" />
					 
					     @error('current_password')
					     	<span class="invalid-feedback">
					     		{{ $message }}
					     	</span>
					     @enderror
					    </div>
					   </div>
					   <div class="form-group row">
					    <label class="col-lg-3 col-form-label">Password Baru:</label>
					    <div class="col-lg-6">
					     <input 
					     	type="password" 
					     	class="form-control @error('password') is-invalid @enderror" 
					     	placeholder="Masukkan Password Baru"
					     	name="password" />
					 
					     @error('password')
					     	<span class="invalid-feedback">
					     		{{ $message }}
					     	</span>
					     @enderror
					    </div>
					   </div>
					   <div class="form-group row">
					    <label class="col-lg-3 col-form-label">Konfirmasi Password:</label>
					    <div class="col-lg-6">
					     <input 
					     	type="password" 
					     	class="form-control @error('password_confirmation') is-invalid @enderror" 
					     	placeholder="Konfirmasi Password Baru"
					     	name="password_confirmation" />
					 
					     @error('password_confirmation')
					     	<span class="invalid-feedback">
					     		{{ $message }}
					     	</span>
					     @enderror
					    </div>
					   </div>
					  </div>

					 </div>
					 <div class="card-footer">
					  <div class="row">
					   <div class="col-lg-6">
					    <button type="submit" class="btn btn-success mr-2">Simpan Perubahan</button>
					   </div>
					  </div>
					 </div>
					</form>
			   </div>
			  </div>
			 </div>
		</div>

@endsection

					
					
