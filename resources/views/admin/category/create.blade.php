@extends('layouts.admin.default')

@section('title', 'Create Category')
@section('pageTitle', 'Create Category')

@section('content')

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Create a New Category</h3>
	</div>

	<form 
		action="{{ route('admin.category.store') }}" 
		method="POST"
		enctype="multipart/form-data">

		@csrf

		<div class="card-body">
			<div class="form-group">
				<label>Name 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="name" 
					class="form-control @error('name') is-invalid @enderror"
					value="{{old('name')}}" 
					placeholder="Enter a new category. " />
				@error('name')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Image 
				<span class="text-danger">*</span></label>
				<input 
					type="file"
					name="image" 
					class="form-control @error('image') is-invalid @enderror" 
					placeholder="Enter a new image." />
				<span class="form-text text-muted">
					Allowed File Extensions: .jpg, .jpeg, .png
				</span>
				@error('image')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		</div>
	</form>

</div>

@endsection