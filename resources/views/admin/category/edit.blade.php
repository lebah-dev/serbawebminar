@extends('layouts.admin.default')

@section('title', 'Edit Category')
@section('pageTitle', 'Edit Category')

@section('content')

<div class="card card-custom gutter-b example example-compact">
	<div class="card-header">
		<h3 class="card-title">Edit Data {{ $category->name}}</h3>
	</div>

	<form 
		action="{{ route('admin.category.update', $category->id)}}" 
		method="POST"
		enctype="multipart/form-data">

	@csrf
	@method('PUT')

		<div class="card-body">
			<div class="form-group">
				<label>Name 
				<span class="text-danger">*</span></label>
				<input 
					type="text"
					name="name" 
					class="form-control @error('name') is-invalid @enderror"
					value="{{ old('name') ?? $category->name }}" 
					placeholder="Enter a new category. " />

				@error('name')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
			<div class="form-group">
				<label>Slug 
				<span class="text-danger">*</span>
				</label>
				<input 
					type="text"
					name="slug" 
					class="form-control"
					value="{{ $category->slug}}" 
					readonly=""
					disabled="disabled" 
					placeholder="Enter a new slug. " />
			</div>
			<div class="form-group">
				<label>Image 
				<span class="text-danger">*</span></label> <br>
				<small class="text-muted">Current image</small><br>
				@if($category->image && file_exists(storage_path('app/public/'.$category->image)))
					<img src="{{$category->getImage()}}" height="100" width="100">
				@else
					<img src="https://via.placeholder.com/96x96" width="96px">
				@endif
				<br><br>

				<input 
					type="file"
					name="image" 
					class="form-control @error('image') is-invalid @enderror" 
					placeholder="Enter a new image." />
				<span class="form-text text-muted">
					Jika gambar tidak ingin dirubah, kolom dapat dikosongkan.
				</span>
				<span class="form-text text-muted">
					Allowed File Extensions: .jpg, .jpeg, .png
				</span>

				@error('image')
					<span class="invalid-feedback">
						{{ $message }}
					</span>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-2">Submit</button>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		</div>
	</form>
</div>

@endsection