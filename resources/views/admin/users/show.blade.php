<table class="table table-bordered">
	<tr>
		<th>Name</th>
		<td>{{ $user->name }}</td>
	</tr>
	<tr>
		<th>Email</th>
		<td>{{ $user->email}}</td>
	</tr>
	<tr>
		<th>Joined</th>
		<td>{{ $user->created_at->diffForhumans()}}</td>
	</tr>
	<tr>
		<th>Total Video</th>
		<td>
			{{ $user->videos->count()}}
		</td>
	</tr>
	<tr>
		<th>Channel</th>
		@if(!empty($user->channel))
			<td>
				<table class="table table-bordered w-100">
					<tr>
						<th>Name</th>
						<th>Created at</th>
						<th>Actions</th>
					</tr>
					<tr>
						<td>{{ $user->channel->name }}</td>
						<td>{{ $user->channel->created_at->diffForhumans() }}</td>
						<td>
							<a 
								href="{{ route('admin.channel.edit', $user->channel->id) }}" 
								class="btn btn-warning"
								title="edit">
								<i class="fa fa-edit"></i>
							</a>

							<form 
								class="d-inline" 
								action="{{ route('admin.channel.destroy', $user->channel->id) }}" 
								method="POST">

								@csrf
								@method('delete')

								<button 
									class="btn btn-danger"
									title="delete">
									<i class="fa fa-trash"></i>
								</button>
							</form>
						</td>
					</tr>
				</table>
			</td>
		@else
			<td>
				<form 
					class="d-inline" 
					action="{{ route('admin.channel.create') }}" 
					method="GET">

					<button
						value="{{ Crypt::encrypt($user->id) }}"
						name="owner" 
						class="btn btn-primary">
						Create Channel
					</button>
				</form>
			</td>
		@endif
	</tr>
</table>