<!DOCTYPE html>
<html lang="en"  >
    <head>
        <meta charset="utf-8"/>
        <title>{{ config('app.name') }} | @yield('title')</title>
        <meta name="description" content="Some description for the page"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        {{-- <link rel="shortcut icon" href="{{ asset('/media/logos/favicon.ico')}}" /> --}}
        <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

        @include('layouts.admin.partials.styles')
    
    </head>

    <body  
      id="kt_body"
      class="quick-panel-right demo-panel-right offcanvas-right header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-minimize-hoverable aside-fixed page-loading">
      <div class="d-flex flex-column flex-root">

        <div class="d-flex flex-column flex-root">
          <div class="d-flex flex-row flex-column-fluid page">

            @include('layouts.admin.partials.sidebar')
            
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
              
              @include('layouts.admin.partials.header')
              
              <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                
                @include('layouts.admin.partials.subheader')
                
                <div class="d-flex flex-column-fluid">

                  <div class="container">
                    
                    @yield('content')

                    
                  
                  </div>
                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
                      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                          <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title"></h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                          </button>
                              </div> 
                              <div class="modal-body">
                                  <div class="d-flex justify-content-center">
                                    <button 
                                      type="button" 
                                      class="btn btn-secondary mr-3 spinner spinner-dark spinner-right pr-15" 
                                      wait-class="spinner spinner-dark spinner-right pr-15">
                                      Loading
                                    </button> 
                                  </div>
                                  
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
              </div>

              @include('layouts.admin.partials.footer')

                    
            
            </div>
          </div>
      </div>
    </div>

    @include('layouts.admin.partials.userPanel')

    @include('layouts.admin.partials.scripts')

    </body>
</html>