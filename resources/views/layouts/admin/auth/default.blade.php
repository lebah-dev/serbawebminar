<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		 <title>{{ config('app.name') }} | @yield('title', $page_title ?? '')</title>
		<meta name="description" content="Login page example" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<link rel="canonical" href="https://keenthemes.com/metronic" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<link href="assets/css/pages/login/login-1.css" rel="stylesheet" type="text/css" />

		@include('layouts.admin.partials.styles')

		{{-- <link rel="shortcut icon" href="assets/media/logos/favicon.ico" /> --}}
		<link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
	</head>

	<body class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
		
		<div class="d-flex flex-column flex-root">
			<div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
				<div class="login-content flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden p-7 mx-auto">
					<div class="d-flex flex-column-fluid flex-center">

						@yield('loginContent')

					</div>

					@include('layouts.admin.auth.partials.footer')
					
				</div>
			</div>
		</div>

		@include('layouts.admin.partials.scripts')

	</body>
</html>