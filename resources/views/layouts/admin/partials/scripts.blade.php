<script>var HOST_URL = "{{ asset('/quick-search')}}";</script>
<script src="{{ asset('assets/admin/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/plugins/custom/prismjs/prismjs.bundle.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/js/scripts.bundle.js')}}" type="text/javascript"></script>
@stack('scripts')

<script>
    jQuery(document).ready(function($){
        $('#myModal').on('show.bs.modal', function(e){
            var button = $(e.relatedTarget);
            var modal = $(this);
            modal.find('.modal-body').load(button.data("remote"));
            modal.find('.modal-title').html(button.data("title"));
        });
    });
</script>



