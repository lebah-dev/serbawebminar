<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
  <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-center">
    <div class="text-dark order-2 order-md-1">
      <span class="text-muted font-weight-bold mr-2">2021©</span>
      <a href="http://keenthemes.com/metronic" target="_blank" class="text-dark-75 text-hover-primary">SerbaWebinar</a>
    </div>
  </div>
</div>