<link href="{{ asset('assets/admin/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/admin/plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/admin/css/style.bundle.css')}}" rel="stylesheet" type="text/css"/>

<link href="{{ asset('assets/admin/css/themes/layout/header/base/light.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/admin/css/themes/layout/header/menu/light.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/admin/css/themes/layout/aside/dark.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/admin/css/themes/layout/brand/dark.css')}}" rel="stylesheet" type="text/css"/>

@stack('styles')