<a 
	href="#myModal"
	data-remote = "/admin/{{request()->segment(3)}}/{{$model->id}}"
	data-toggle = "modal"
	data-target = "#myModal"
	data-title = 'Show Playlist "{{$model->name}}"'
	class="btn btn-primary"
	title="show">
	<i class="fa fa-eye"></i>
</a>

<a 
	href="/admin/{{request()->segment(3)}}/{{$model->id}}/edit" 
	class="btn btn-warning"
	title="edit">
	<i class="fa fa-edit"></i>
</a>

<button 
	class="btn btn-danger"
	id="delete"
	data-id="{{$model->id}}"
	title="delete">
	<i class="fa fa-trash"></i>
</button>