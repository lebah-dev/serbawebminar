<a 
	href="/admin/{{request()->segment(3)}}/{{$model->id}}/edit" 
	class="btn btn-warning"
	title="edit">
	<i class="fa fa-edit"></i>
</a>

<button 
	class="btn btn-danger"
	id="delete"
	data-id="{{$model->id}}"
	title="delete">
	<i class="fa fa-trash"></i>
</button>