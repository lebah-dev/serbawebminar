<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'User',
        	'email' => 'user@email.com',
        	'email_verified_at' => Carbon::now(),
        	'password' => bcrypt('12345678'),
        	'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now()
        ]);
    }
}
