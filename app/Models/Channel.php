<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    use HasFactory;
    protected $guarded = [];


    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function getImage()
    {
    	return route('storage.retrieve', encrypt($this->cover_image));
    }

    public function userSubscribes()
    {
        return $this->hasMany(UserSubscribe::class);
    }

    public function playlists()
    {
        return $this->hasMany(Playlist::class);
    }
}
