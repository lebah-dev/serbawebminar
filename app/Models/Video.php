<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Video extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function getImage()
    {
        if ($this->video_type === 'upload') {
            return route('storage.retrieve', encrypt($this->thumbnail));
            
        } else {
            return $this->thumbnail;
        }
    }

    public function getVideo()
    {
        if ($this->video_type === 'upload') {
            return route('storage.retrieve', encrypt($this->path));

        } else {
            return $this->path;
        }
    }

    public function countLike()
    {
        return $this->userLikeVideos()->where('like', 'like')->get()->count();
    }

    public function countDislike()
    {
         return $this->userLikeVideos()->where('like', 'dislike')->get()->count();
    }

    public function countSubscribe()
    {
         return $this->createBy->channel->userSubscribes()->get()->count();
    }

    public function createBy()
    {
    	return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function userLikeVideos()
    {
        return $this->hasMany(UserLikeVideo::class);
    }

    public function tagVideos()
    {
        return $this->hasMany(TagVideo::class);
    }

    public function videoComments()
    {
        return $this->hasMany(Comment::class);
    }

    public function videoCommentParents()
    {
        return $this->videoComments()->whereNull('parent_id');
    } 

    public function playlists()
    {
        return $this->hasMany(Playlist::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
    
}
