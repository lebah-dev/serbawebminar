<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;

    protected $guarded = [];


    public function getImage()
    {
    	return route('storage.retrieve', encrypt($this->image));
    }
}
