<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_login_at' => 'datetime'
    ];

    public function videos()
    {
        return $this->hasMany(Video::class, 'created_by');
    }

    public function userLikeVideos(){
        return $this->hasMany(UserLikeVideo::class);
    }

    public function channel()
    {
        return $this->hasOne(Channel::class);
    }

    public function user_avatar()
    {
        return $this->hasOne(UserAvatar::class);
    }

    public function userComments()
    {
        return $this->hasMany(Comment::class);
    }

    public function userLikeComments()
    {
        return $this->hasMany(UserLikeComment::class);
    }

    public function userSubscribes()
    {
        return $this->hasMany(UserSubscribe::class);
    }

    public function user_socials()
    {
        return $this->hasMany(UserSocial::class);
    }
    
}
