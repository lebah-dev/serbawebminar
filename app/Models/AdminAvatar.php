<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminAvatar extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function admin()
    {
    	return $this->belongsTo(Admin::class);
    }

    public function getImage()
    {
        return route('storage.retrieve', encrypt($this->thumbnail));
    }
}
