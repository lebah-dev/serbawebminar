<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function countLike()
    {
        return $this->userLikeComments()->where('like', 'like')->get()->count();
    }

    public function countDislike()
    {
         return $this->userLikeComments()->where('like', 'dislike')->get()->count();
    }


    public function parents()
    {
    	return $this->hasMany(Comment::class, 'parent_id');
    }

    public function belongparent()
    {
    	return $this->belongsTo(Comment::class, 'parent_id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function video()
    {
    	return $this->belongsTo(Video::class);
    }

    public function userLikeComments()
    {
        return $this->hasMany(UserLikeComment::class);
    }

}
