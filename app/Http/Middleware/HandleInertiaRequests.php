<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Inertia\Middleware;
use Illuminate\Support\Facades\Auth;


class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request)
    {
        return array_merge(parent::share($request), [
            'loggedIn' => Auth::check(),
            'username' => Auth::check() ? Auth::user()->name : '',
            'last_login_at' => Auth::check() ? optional(Auth::user()->last_login_at)->diffForHumans() : '',
            'created_at' => Auth::check() ? tglIndo(Auth::user()->created_at) : '',
            'logo' => asset('images/logo.png'),
            'user_avatar' => Auth::check()
                ? (Auth::user()->user_avatar
                    ? route('storage.retrieve', encrypt(Auth::user()->user_avatar->thumbnail))
                    : asset('images/resources/sn.png'))
                : asset('images/resources/sn.png'),
            'avatar' => Auth::check()
                ? (Auth::user()->user_avatar
                    ? route('storage.retrieve', encrypt(Auth::user()->user_avatar->thumbnail))
                    : 'https://ui-avatars.com/api/?rounded=true&name=' . Auth::user()->name . '')
                : 'https://ui-avatars.com/api/?rounded=true',
        ]);
    }
}
