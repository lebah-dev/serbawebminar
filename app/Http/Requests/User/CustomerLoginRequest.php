<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class CustomerLoginRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email',
            'password' => [
                'required'
            ]
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Email Dibutuhkan!',
            'email.string' => 'Email harus string!',
            'password.required' => 'Password Dibutuhkan!'
        ];
    }
}
