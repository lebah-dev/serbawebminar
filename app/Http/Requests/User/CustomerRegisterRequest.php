<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRegisterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => [
                'required', 'min:8', 'max:40', 'string', 'confirmed'
            ]
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nama Lengkap Dibutuhkan!',
            'name.string' => 'Nama harus berupa string!',
            'email.required' => 'Email Dibutuhkan!',
            'email.string' => 'Email harus string!',
            'email.unique' => 'Email tersebut sudah terdaftar!',
            'password.required' => 'Password Dibutuhkan!',
            'password.string' => 'Password harus string!',
            'password.min' => 'Kata sandi minimal 8 karakter!',
            'password.max' => 'Kata sandi maximal 40 karakter!',
            'password.confirmed' => 'Konfirmasi Password Tidak Cocok!',
            'password.regex' => 'Minimal 8 characters, termasuk 1 huruf, 1 angka.'
        ];
    }
}
