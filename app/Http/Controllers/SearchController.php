<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Video, Channel};
use App\Http\Resources\{VideoResource, ChannelResource};
use Inertia\Inertia;

class SearchController extends Controller
{
    public function index(Request $request)
    {
    	$video = $request->video;
        $type = $request->type;
        $upload_date = $request->upload_date;
    	$data = '';

    	if ($video) {
            // 
    		$data = Video::where('title', 'like', '%'.$video.'%');
    	}

        if ($type && $video) {
            $data = Video::where('title', 'like', '%'.$video.'%')
                        ->where('video_type', 'like', '%'.$type.'%');
        }

        if ($upload_date && $video) {

            if ($upload_date === 'today') {
                $data = Video::where('title', 'like', '%'.$video.'%')
                            ->whereDate('created_at', \Carbon\Carbon::now()->today());
            }

            if ($upload_date === 'month') {
                $data = Video::where('title', 'like', '%'.$video.'%')
                            ->whereMonth('created_at', \Carbon\Carbon::now()->month());
            }

            if ($upload_date === 'year') {
                $data = Video::where('title', 'like', '%'.$video.'%')
                        ->whereYear('created_at', \Carbon\Carbon::now()->year());
            }
            
        }

        if ($data->count() > 0) {
            return Inertia::render('Search', [
                'videos' =>  VideoResource::collection($data->get()),
                'keyword' => $video ? $video : '',
                'type' => 'video'
            ]);
        } else {
            $channels = $this->searchChannel($request);

            return Inertia::render('Search', [
                'videos' =>  ChannelResource::collection($channels->get()),
                'keyword' => $video ? $video : '',
                'type' => 'channel'
            ]);
        }

    	
    }
    
    public function searchChannel($request)
    {
        $data = Channel::where('name', 'like', '%'.$request->video.'%');

        return $data;
    }
}
