<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;
use App\Http\Resources\BannerResource;
use Inertia\Inertia;

class BannerController extends Controller
{
    public function show($slug)
    {
    	$banner = Banner::where('slug', $slug)->get();

    	return Inertia::render('BannerShow', [
    		'banner' => BannerResource::collection($banner)
    	]);
    }
}
