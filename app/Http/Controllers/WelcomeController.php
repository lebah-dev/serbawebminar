<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Http\Controllers\Controller;
use App\Http\Resources\{VideoResource, ChannelResource, BannerResource};
use App\Models\{ Video, Channel, Banner };

class WelcomeController extends Controller
{
    public function index()
    {
        $new_videos = Video::orderBy('created_at', 'desc')->take(8)->get();
        $video_subscribtions = Video::orderByRaw('RAND()')->take(8)->get();
        $popular_videos = Video::orderBy('views_count', 'desc')->take(8)->get();
        $channels = Channel::with('userSubscribes')->take(8)->get()->sortByDesc(function($query)
        {
            return $query->userSubscribes->count();
        });

        $banners = Banner::where('status', 'on')->get();


        return Inertia::render('Welcome', [
            'new_videos' => VideoResource::collection($new_videos),
            'video_subscribtions' => VideoResource::collection($video_subscribtions),
            'popular_videos' => VideoResource::collection($popular_videos),
            'channels' => ChannelResource::collection($channels),
            'banners' => BannerResource::collection($banners)
        ]);    
    }
}
