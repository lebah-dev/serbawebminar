<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthenticateUserController extends Controller
{

    public function checkAuth()
    {
        if (Auth::user()) {
            $name = str_word_count(Auth::user()->name) > 1.5 ? substr(Auth::user()->name,0,5) : Auth::user()->name;
            $auth = true;
        } else {
            $name = null;
            $auth = false;
        }

        $data = [
            'name' => $name,
            'auth' => $auth,
        ];

        return json_encode($data);
    }
}
