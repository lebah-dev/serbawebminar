<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;

class StorageController extends Controller
{
	/**
	 * @param string $encryptedPath
	 * @return string
	 */
	public function retrieve(string $encryptedPath)
	{
		return Storage::get(decrypt($encryptedPath));
	}
}