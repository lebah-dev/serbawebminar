<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\{Category, Video };
use App\Http\Resources\{ CategoryResource, VideoResource };

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with('videos')->paginate(12);

        return Inertia::render('Category/Index', [
            'categories' => CategoryResource::collection($categories)->response()->getData(true)
        ]);
    }

    public function APIIndex()
    {
        $data = [];

        $categories = Category::orderBy('created_at', 'desc')->get();

        foreach ($categories as $category) {
            array_push($data, [
                'id' => $category->id,
                'name' => $category->name,
                'slug' => $category->slug
            ]);
        }

        return response([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $category = Category::where('slug', $slug)->with('videos')->get();
        $videos = VideoResource::collection($category->first()->videos->paginate(8));
        
        return Inertia::render('Category/Show', [
            'category' => CategoryResource::collection($category),
            'videos' => $videos->response()->getData(true)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
