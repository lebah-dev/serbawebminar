<?php

namespace App\Http\Controllers\Footer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MediaSosialController extends Controller
{
    public function index()
    {
    	return Inertia::render('Footer/MediaSosial');
    }
}
