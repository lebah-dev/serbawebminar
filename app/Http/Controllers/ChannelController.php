<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Channel, Video};
use Inertia\Inertia;
use Auth, DB, Image, Storage, Str;
use App\Http\Resources\{ChannelResource, VideoResource};

class ChannelController extends Controller
{
    public function __invoke(Request $request)
    {
        $channel = Channel::with('user.videos')->where('links', $request->channel)->firstOrFail();

        return Inertia::render('ChannelVideo', [
            'channel' => new ChannelResource($channel)
        ]);    
    }

    public function search(Request $request)
    {
        $channel_id = $request->channel;

        $videos = Video::where('title', 'LIKE', "%$request->keyword%")
        ->whereHas('createBy.channel', function($query) use ($channel_id){
            $query->where('id', $channel_id);
        })->orderBy('created_at', 'desc')->get();

        return VideoResource::collection($videos);
    }
}
