<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{User, Video};
use Inertia\Inertia;
use Auth, DB, Image, Storage, Str;

class UserNotificationController extends Controller
{
    public function getUnreadNotification()
    {
        $reusable = [];
        
        $notifications = Auth::user()->unreadNotifications()->select('id', 'data', 'created_at')->get();
        
        foreach ($notifications as $notification) {
            array_push($reusable,[
                'id' => $notification->id,
                'data' => $notification->data,
                'created_at' => tglWaktuIndo($notification->created_at)
            ]);
        }

        return json_encode($reusable);
    }

    /**
     * Ubah notifikasi menjadi sudah dibaca
     * 
     * @param int  $id
     */
    public function markReadNotif($id)
    {
        Auth::user()->unreadNotifications()->where('id', $id)->first()->update(['read_at' => now()]);

        return response()->json(['success' => 'Successfully read the notif']);
    }

    /**
     * Ubah semua data notifikasi menjadi terbaca
     */
    public function markReadAllNotif()
    {
        Auth::user()->notifications->markAsRead();

        return response()->json(['success' => 'Successfully mark read all notif']);
    }
}
