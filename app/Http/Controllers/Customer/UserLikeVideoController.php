<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{User, Video};
use Inertia\Inertia;
use Auth, DB, Image, Storage, Str;

class UserLikeVideoController extends Controller
{
    public function store(Request $request)
    {
        $like = Auth::user()->userLikeVideos()->where('video_id', $request->video)->first();
        $video = Video::where('id', $request->video)->firstOrFail();

        if ($like) {
            $like->like = $request->respons;
            $like->save();

        } else {
            Auth::user()->userLikeVideos()->create([
                'video_id' => $request->video,
                'like' => $request->respons
            ]);
        }

        return response()->json([
            'status' => 'success', 
            'countLike' => $video->countLike(),
            'countDislike' => $video->countDislike(),
        ]);
    }
}
