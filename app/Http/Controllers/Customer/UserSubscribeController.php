<?php

namespace App\Http\Controllers\Customer;

use App\Http\Requests\User\CustomerCommentRequest;
use App\Notifications\User\UserSubscribeNotification;
use App\Http\Controllers\Controller;
use App\Http\Resources\VideoCommentResource;
use Illuminate\Http\Request;
use App\Models\{Channel, Video, Comment, UserSubscribe};
use Inertia\Inertia;
use Auth, DB, Image, Storage, Str, Notification;

class UserSubscribeController extends Controller
{
    public function store(Request $request)
    {
        $subscribe = UserSubscribe::where('channel_id', $request->channel)->where('user_id', Auth::user()->id)->first();
        $channel = Channel::where('id', $request->channel)->firstOrFail();

        if ($subscribe) {
            $subscribe->delete();

            Notification::send($channel->user, new UserSubscribeNotification(Auth::user()->name, 'unsubscribe'));

        } else {
            $userSubscribe = new UserSubscribe;
            $userSubscribe->channel_id = $request->channel;
            $userSubscribe->user_id = Auth::user()->id;
            $userSubscribe->save();

            Notification::send($channel->user, new UserSubscribeNotification(Auth::user()->name, 'subscribe'));

        }

        return response()->json([
            'status' => 'success',
            'countSubscribe' => $channel->userSubscribes->count()
        ]);
    }
}
