<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class AvatarController extends Controller
{
    public function APIAvatar(Request $request)
    {
        return response()->json([
            'path' => $request->user()->user_avatar
                ? route('storage.retrieve', encrypt($request->user()->user_avatar->thumbnail))
                : asset('images/resources/sn.png'),
            'name' => $request->user()->name,
            'user_id' => Auth::id()
        ]);
    }

    public function store(Request $request)
    {
        $thumb = Image::make($request->thumbnail)->fit(60)->encode('jpg', 75);
        $origin = Image::make($request->origin)->fit(1024)->encode('jpg', 75);
        $nameFinal = Str::random(40). '.jpg';
        $path = 'avatar/' . $nameFinal;
        $thumbPath = 'avatar-thumb/' . $nameFinal;

        Storage::put($path, $origin->__toString());
        Storage::put($thumbPath, $thumb->__toString());

        $request->user()->user_avatar()->updateOrCreate([
            'thumbnail' => $thumbPath,
            'main_path' => $path
        ]);

        return response()->json(['status' => 'success']);
    }
}
