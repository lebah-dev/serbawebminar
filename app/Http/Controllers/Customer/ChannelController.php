<?php

namespace App\Http\Controllers\Customer;

use App\Http\Requests\User\CustomerChannelRequest;
use App\Http\Controllers\Controller;
use App\Models\Channel;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;
use Intervention\Image\Facades\Image;

class ChannelController extends Controller
{
    public function index()
    {
        $channel = Auth::user()->channel;
        if (!$channel) return redirect()->route('customer.create.channel');

        return redirect()->route('customer.edit.channel', $channel);
    }

    public function create()
    {
        return Inertia::render('ChannelCreate');
    }

    public function store(CustomerChannelRequest $request)
    {
        $image = Image::make($request->image)->encode('jpg', 75);
        $path = 'channel/' . Str::random(40). '.jpg';

        Storage::put($path, $image->__toString());

        $request->user()->channel()->create([
            'name' => $request->name,
            'description' => $request->description,
            'links' => $request->links,
            'business' => $request->bussiness,
            'cover_image' => $path
        ]);

        return response()->json(['status' => 'success']);

    }

    public function edit(Channel $channel)
    {
        $data = [
            'id' => $channel->id,
            'cover_image' => route('storage.retrieve', encrypt($channel->cover_image)),
            'name' => $channel->name,
            'description' => $channel->description,
            'bussiness' => $channel->business,
            'links' => $channel->links
        ];

        return Inertia::render('ChannelEdit', [
            'channel' => $data
        ]);
    }

    public function update(CustomerChannelRequest $request, Channel $channel)
    {
        if (!$channel->cover_image) {
            $channel->cover_image = 'channel/' . Str::random(40). '.jpg';
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image)) {
            $image = Image::make($request->image)->encode('jpg', 75);
            Storage::put($channel->cover_image, $image->__toString());
        }

        $channel->update($request->all());

        return redirect()->route('customer.edit.channel', $channel->id);
    }
}
