<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{User, Video, Playlist};
use App\Http\Resources\PlaylistResource;
use Inertia\Inertia;
use Auth, DB, Image, Storage, Str;

class UserPlaylistController extends Controller
{
    public function index()
    {
        if (Auth::user()->channel) {

            $playlists = Auth::user()->channel->playlists()->get();
    	
            return Inertia::render('ManagePlaylist', [
                'playlists' => PlaylistResource::collection($playlists),
            ]);    

        } else {
            return redirect()->route('customer.create.channel');
        }
    	
    }

    public function APIIndex()
    {   
        if (Auth::user()->channel) {
            $playlists = Auth::user()->channel->playlists()->get();
        } else {
            $playlists = [];
        }

        return json_encode($playlists);
    }

    public function store(Request $request)
    {
        if (Auth::user()->channel) {

            $playlist = new Playlist;
            $playlist->channel_id = Auth::user()->channel->id;
            $playlist->name = $request->name;
            $playlist->privacy_setting = $request->privacy_setting;
            $playlist->save();

            $playlist->videos()->attach([
                'video_id' => $request->video
            ]);

            return response()->json("Video successfully add to playlist", 200);
        } else {
            return response()->json("Create your channel", 404);
        }
    }

    public function storeVideoPlaylist(Request $request)
    {
        if (Auth::user()->channel) {
            $playlist = Playlist::where('id', $request->playlist)->firstOrFail();

            $video = $playlist->videos()->where('video_id', $request->video)->first();

            if ($video) {
                return response()->json("video is already in the playlist", 404);
            } else {
                $playlist->videos()->attach([
                    'video_id' => $request->video
                ]);

                return response()->json("Video successfully add to playlist", 200);

            }

        } else {
            return response()->json("Create your channel", 404);
        }
    }

    public function destroy(Request $request)
    {
        $playlist = Playlist::where('id', $request->playlist)->firstOrFail();

        $playlist->videos()->detach([
            'video_id' => $request->video
        ]);

        return redirect()->route('customer.account');

    }
}
