<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Inertia\Inertia;
use Auth, Hash;

class PasswordController extends Controller
{
    public function index()
    {
        return Inertia::render('ChangePassword');   
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'current_password' => 'required|string',
            'password' => [
                'required', 'min:8', 'max:40', 'string', 'confirmed',
                'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
            ]
        ], [
            'password.required' => 'Password Dibutuhkan!',
            'password.string' => 'Password harus string!',
            'password.min' => 'Kata sandi minimal 8 karakter!',
            'password.max' => 'Kata sandi maximal 40 karakter!',
            'password.confirmed' => 'Konfirmasi Password Tidak Cocok!',
            'password.regex' => 'Minimal 8 characters, termasuk 1 huruf, 1 angka.'
        ]);

        if (Hash::check($request->current_password, Auth::user()->password)) {
            $request->user()->fill(['password' => Hash::make($request->password)])->save();

            return redirect()->route('customer.account')->withSuccess('Berhasil Mengubah Kata Sandi!');
            
        } else {
            return response()->json("Gagal Mengubah Kata Sandi!", 400);
        }
    }
}
