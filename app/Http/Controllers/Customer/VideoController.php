<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\{ File, Http };
use Illuminate\Http\Request;
use App\Models\{User, Video, Category};
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DateInterval;


class VideoController extends Controller
{
    public function index()
    {
        if (Auth::user()->channel) {
            return Inertia::render('UploadVideo');
        } else {
            return redirect()->route('customer.create.channel');
        }
    }

    public function store(Request $request)
    {
        $id_categories = [];

        foreach ($request->categories as $category) {
            array_push($id_categories, $category['id']);
        }

        $video = new Video;
        $video->video_type = $request->video_type;

        if ($request->video_type == 'youtube') {
            $regex = "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/";

            $url = $request->video;
            preg_match($regex, $url, $matches);
            $videoId = $matches[1]; 
            $path = $videoId;

            $youtubeKey = config('services.youtube.key');
            $response = Http::get('https://www.googleapis.com/youtube/v3/videos?id='.$videoId.'&key='.$youtubeKey.'&part=snippet,contentDetails,statistics,status')->json();

            $interval = new DateInterval($response['items'][0]['contentDetails']['duration']);
            $seconds = $interval->days * 86400 + $interval->h * 3600 + $interval->i * 60 + $interval->s;

            $title = $response['items'][0]['snippet']['title'];
            $about = $response['items'][0]['snippet']['description'];
            $thumbnail = $response['items'][0]['snippet']['thumbnails']['high']['url'];
            $duration = $seconds;

        } else {

            $title = $request->title;
            $about = $request->about;
            $duration = $request->duration;

            if ($request->file_type == 'image/gif') {

                $data = substr($request->thumbnail, strpos($request->thumbnail, ',') + 1);
                $thumbnail = 'video_thumbnail/' . Str::random(40). '.gif';
                $data = base64_decode($data);

                Storage::put($thumbnail, $data);

            } else {
                $thumbnail = 'video_thumbnail/' . Str::random(40). '.jpg';
                $thumb = Image::make($request->thumbnail)->fit(480, 360)->encode('jpg', 75);
    
                Storage::put($thumbnail, $thumb->__toString());

            }


            if (preg_match('/^data:video\/(\w+);base64,/', $request->video)) {
                $data = substr($request->video, strpos($request->video, ',') + 1);
                $path = 'video/' . Str::random(40). '.mp4';
                $data = base64_decode($data);

                Storage::put($path, $data);
            } 
        }

        $video_exist = Video::where('slug', '=', Str::slug($title))->count();
        
        if ($video_exist) {   
            $video_title = $this->generateCodeProduct($title);
            $video->slug = Str::slug($video_title);
        } else {
            $video->slug = Str::slug($title);
        }

        $video->title = $title;
        $video->about = $about;
        $video->thumbnail = $thumbnail;
        $video->privacy_setting = $request->privacy_setting;
        $video->path = $path;
        $video->views_count = 1;
        $video->created_by = Auth::user()->id;
        $video->duration = $duration;
        $video->save();

        $video->categories()->attach($id_categories);

        return redirect()->route('customer.account');
    }

    protected function generateCodeProduct($name)
    {
        $video_name =  $name . " " . rand(10000, 99999);

        return $video_name;
    }

    public function watch(Video $video)
    {
        $video->video = $video->getVideo();
        $video->thumbnail = $video->getImage();
        return Inertia::render('WatchVideo', [
            'video' => $video,
            'channel' => $video->createBy->channel->name,
            'avatar_user' => $video->createBy->user_avatar ? $video->createBy->user_avatar->getImage() : asset('images/resources/user-img.png'),
        ]);
    }

    public function edit(Video $video)
    {
        $data = [
            'id' => $video->id,
            'video_type' => $video->video_type,
            'title' => $video->title,
            'slug' => $video->slug,
            'path' => $video->video_type == 'upload' ? route('storage.retrieve', encrypt($video->path)) : $video->path,
            'about' => $video->about,
            'thumbnail' => route('storage.retrieve', encrypt($video->thumbnail)),
            'privacy_setting' => $video->privacy_setting,
            'category' => $video->categories()->get()
        ];
        return Inertia::render('EditVideo', [
            'video' => $data
        ]);
    }

    public function update(Request $request, Video $video)
    {
        $id_categories = [];

        foreach ($request->categories as $category) {
            array_push($id_categories, $category['id']);
        }

        $video->video_type = $request->video_type;

        $thumbnail = $video->thumbnail ?? null;

        if ($request->video_type == 'youtube') {
            $regex = "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/";

            $url = $request->video;
            preg_match($regex, $url, $matches);
            $videoId = $matches[1]; 
            $path = $videoId;

            $youtubeKey = config('services.youtube.key');
            $response = Http::get('https://www.googleapis.com/youtube/v3/videos?id='.$videoId.'&key='.$youtubeKey.'&part=snippet,contentDetails,statistics,status')->json();

            $interval = new DateInterval($response['items'][0]['contentDetails']['duration']);
            $seconds = $interval->days * 86400 + $interval->h * 3600 + $interval->i * 60 + $interval->s;

            $title = $response['items'][0]['snippet']['title'];
            $about = $response['items'][0]['snippet']['description'];
            $thumbnail = $response['items'][0]['snippet']['thumbnails']['high']['url'];
            $duration = $seconds;

            $video->path = $path;

        } else {

            $title = $request->title;
            $about = $request->about;
            $duration = $request->duration;

            if (preg_match('/^data:image\/(\w+);base64,/', $request->thumbnail)) {
                $thumb = Image::make($request->thumbnail)->fit(480, 360)->encode('jpg', 75);
                $thumbnail = 'video_thumbnail/' . Str::random(40). '.jpg';
                Storage::delete($video->thumbnail);
                Storage::put($thumbnail, $thumb->__toString());

            }
            
            if (preg_match('/^data:video\/(\w+);base64,/', $request->video)) {
                $data = substr($request->video, strpos($request->video, ',') + 1);
                
                $path = 'video/' . Str::random(40). '.mp4';

                $data = base64_decode($data);

                Storage::delete($video->path);
                Storage::put($path, $data);

                $video->path = $path;

            } 
        }

        $video_exist = Video::where('slug', '=', Str::slug($title))->count();
        
        if ($video_exist) {   
            $video_title = $this->generateCodeProduct($title);
            $video->slug = Str::slug($video_title);
        } else {
            $video->slug = Str::slug($title);
        }

        $video->title = $title;
        $video->about = $about;
        $video->thumbnail = $thumbnail;
        $video->privacy_setting = $request->privacy_setting;
        $video->views_count = 1;
        $video->created_by = Auth::user()->id;
        $video->duration = $duration;
        $video->save();

        $video->categories()->sync($id_categories);


        return redirect()->route('customer.account');
    }

    public function destroy(Video $video)
    {
        if ($video->video_type == 'upload') {
            Storage::delete($video->path);
        }
        Storage::delete($video->thumbnail);
        $video->delete();
        
        return redirect()->route('customer.account');
    }


}
