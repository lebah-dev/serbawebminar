<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Video;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class MyAccountController extends Controller
{
    public function index()
    {
    	$videos = Video::where('created_by', Auth::user()->id)->get();
        $videos->transform(function ($video) {
            $video->video = $video->getVideo();
            $video->thumbnail = $video->getImage();
            return $video;
        });
    	
        return Inertia::render('Account', [
            'username' => Auth::user()->name,
            'last_login_at' => optional(Auth::user()->last_login_at)->diffForHumans(),
            'created_at' => tglIndo(Auth::user()->created_at),
            'videos' => $videos,
        ]);    
    }

    
}
