<?php

namespace App\Http\Controllers\Customer;

use App\Notifications\User\UserCommentNotification;
use App\Http\Requests\User\CustomerCommentRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\VideoCommentResource;
use Illuminate\Http\Request;
use App\Models\{Channel, Video, Comment};
use Inertia\Inertia;
use Auth, DB, Image, Storage, Str, Notification;

class UserVideoCommentController extends Controller
{
    public function storeNewComment(CustomerCommentRequest $request)
    {
        $comment = new Comment;
        $comment->user_id = Auth::user()->id;
        $comment->video_id = $request->video_id;
        $comment->content = $request->comment;
        $comment->save();

        Notification::send($comment->video->createBy, new UserCommentNotification($comment));

        return response()->json(['status' => 'success']);

    }

    public function storeReplyComment(CustomerCommentRequest $request)
    {
        $comment = new Comment;
        $comment->user_id = Auth::user()->id;
        $comment->video_id = $request->video_id;
        $comment->parent_id = $request->parent_id;
        $comment->content = $request->comment;
        $comment->save();

        Notification::send($comment->video->createBy, new UserCommentNotification($comment));

        return response()->json(['status' => 'success']);

    }

    public function storeResponsComment(Request $request)
    {
        $like = Auth::user()->userLikeComments()->where('comment_id', $request->comment)->first();

        if ($like) {
            $like->like = $request->respons;
            $like->save();

        } else {
            Auth::user()->userLikeComments()->create([
                'comment_id' => $request->comment,
                'like' => $request->respons
            ]);
        }

        return response()->json(['status' => 'success']);

    }
}
