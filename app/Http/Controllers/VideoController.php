<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Channel, Video};
use Inertia\Inertia;
use Auth, DB, Image, Storage, Str;
use App\Http\Resources\VideoResource;

class VideoController extends Controller
{
    public function __invoke(Request $request)
    {
        $channel = Channel::with('user.videos')->where('links', $request->channel)->firstOrFail();
        $video = $channel->user->videos()->where('slug', $request->video)->firstOrFail();

        $video->views_count = $video->views_count + 1;
        $video->save();
        
        $other_videos = $channel->user->videos()->where('slug', '!=', $request->video)->limit(8)->get();

        return Inertia::render('ShowVideo', [
            'channel' => $channel,
            'video' => new VideoResource($video),
            'other_videos' => VideoResource::collection($other_videos)
        ]);    
    }
}
