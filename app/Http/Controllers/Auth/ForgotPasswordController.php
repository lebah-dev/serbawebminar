<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use App\Traits\ResetsPasswords;
use Inertia\Inertia;
use Auth;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use ResetsPasswords;

    public function __construct()
    {
        $this->broker = 'users';
    }

    protected function guard()
    {
        return Auth::guard();
    }

    public function showLinkRequestForm()
    {
        return Inertia::render('ForgetPassword'); 
    }

}
