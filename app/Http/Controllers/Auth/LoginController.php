<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\User\{CustomerLoginRequest, ResendEmailVerificationRequest};
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Mail\RegistrationConfirmation;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use Inertia\Inertia;
use Auth, Mail;

class LoginController extends Controller
{

    public function index()
    {
        return Inertia::render('Login');    
    }

    protected function guard()
    {
        return Auth::guard();
    }

    public function login(CustomerLoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if ($user) {
            if ($user->email_verified_at) {
                if(Hash::check($request->password, $user->password)){
                    if ($this->guard()->attempt([
                        'email' => $request->email, 
                        'password' => $request->password,
                    ])) {
    
                        $request->session()->regenerate();
    
                        $user->last_login_at = now();
                        $user->save();
            
                        return response()->json("successfully login user", 200);
                        
                    } else {
                        return response()->json("Authenticated", 400);
                    }
                } else {
                    return response()->json("Cek your email or password is wrong", 400);
                }
            } else {
                return response()->json([
                    'message' => "Please check your email to activate account",
                    'email' => $request->email
                ], 400);
            }
        } else {
            return response()->json("Your Email not registered", 400);
        }

    }

    public function resendVerifyEmail(ResendEmailVerificationRequest $request)
    {
        $user = User::where('email', $request->email)->firstOrFail();

        Mail::to($user->email)->send(new RegistrationConfirmation($user));

        return response()->json("successfully resend link", 200);

    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        return redirect('/');
    }
}
