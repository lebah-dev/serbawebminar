<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\User\CustomerRegisterRequest;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Mail\RegistrationConfirmation;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Auth, DB, Mail;

class RegisterController extends Controller
{
    public function index()
    {
        return Inertia::render('SignUp');    
    }

    public function store(CustomerRegisterRequest $request)
    {
        DB::beginTransaction();

        try {	
            $user = new User;
            $user->name = $request->name;
            $user->email = strtolower($request->email);
            $user->password = Hash::make($request->password);
            $user->save();

            DB::commit();

            Mail::to($user->email)->send(new RegistrationConfirmation($user));

            return response()->json("successfully register user");

        } catch (Exception $e) {
            DB::rollback();
        }
    }

    public function confirmEmail(Request $request)
    {
        $user = User::where('email', $request->email)->firstOrFail();
        $user->email_verified_at = now();
        $user->save();

        return redirect()->route('login');
    }
}
