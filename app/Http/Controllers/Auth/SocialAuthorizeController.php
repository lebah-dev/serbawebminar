<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{User, UserSocial};
use Laravel\Socialite\Facades\Socialite;
use Auth, Hash;

class SocialAuthorizeController extends Controller
{
    public function redirectToProvider(Request $request)
    {
        return Socialite::driver($request->social)->redirect();
    }

    /**
     * Obtain the user information from Social.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request)
    {
        $social = Socialite::driver($request->social)->user();

        $usersocial = UserSocial::where('social_id', $social->getId())
        						->where('social_driver', $request->social)
        						->get();

        return $usersocial->count() ? $this->login($usersocial->first()->user) 
        						: $this->createUser($social, $request->social);
    }

    /**
     * Login masuk ke dalam sistem.
     */
    protected function login($user)
    {
    	Auth::login($user, true);
        
        return redirect()->route('customer.account');
    }

    /**
     * Cek email dari social ke database user, apakah ada atau tidak.
     */
    protected function checkExistEmail($email)
    {
    	return User::where('email', $email)->get();
    }

    /**
     * Membuat user baru dari data link social
     */
    protected function createUser($social, $driver)
    {
        $cekSocial = $this->checkExistEmail($social->getEmail());

        if ($cekSocial->count()) {
        	$this->storeUsertoUserSocial($cekSocial->first(), $social, $driver);

            Auth::login($cekSocial->first(), true);
        } else {
        	$user = $this->storeUser($social);
        	$this->storeUsertoUserSocial($user, $social, $driver);

        	Auth::login($user);
        }

    	return redirect()->route('customer.account')->withSuccess('Meregistrasi Account!');
    }

    /**
     * Menyimpan data dari social ke table User.
     */
    protected function storeUser($social)
    {
        return User::create([
            'name' => $social->getName(),
            'email' => $social->getEmail(),
            'password' => Hash::make('qwerty'),
            'email_verified_at' => now()
        ]);
    }

    /**
     * Menyimpan data user untuk dimasukkan ke table UserSocial.
     */
    protected function storeUsertoUserSocial($user, $social, $driver)
    {
    	$user->user_socials()->create([
        	'social_id' => $social->getId(),
        	'social_driver' => $driver
        ]);
    }
}
