<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{ File, Http };
use App\Models\{ Video, User, Category };
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use DateInterval;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.video.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::select('id', 'name')
                    ->whereHas('channel')
                    ->get();
        return view('admin.video.create',[
            'users' => $users,
            'categories' => Category::orderBy('name', 'asc')->get() 
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'video_for' => 'required',
            'video_type' => 'required',
            'privacy_setting' => 'required'
        ];

        if ($request->video_type == 'youtube') {
            $rules += [
                'pathYoutube' => [
                    'required', 
                    "regex: /^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/"
                ],
            ]; 
        } 

        if ($request->video_type == 'upload') {
            $rules += [
                'pathUpload' => 'required|mimes:mp4',
                'title' => 'required',
                'thumbnail' => 'image|mimes:jpeg,png,jpg|max:2048|dimensions:width=480,height=360',
                'about' => 'required',
            ]; 
        } 

        $request->validate($rules);

        $title = null;
        $path = null;
        $duration = null;
        $about = null;
        $thumbnail = null;

        $categories = Category::find($request->categories);

        if ($request->video_type == 'youtube') {

            $regex = "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/";

            $url = $request->pathYoutube;
            preg_match($regex, $url, $matches);
            $videoId = $matches[1]; 
            $path = $videoId;

            $youtubeKey = config('services.youtube.key');
            $response = Http::get('https://www.googleapis.com/youtube/v3/videos?id='.$videoId.'&key='.$youtubeKey.'&part=snippet,contentDetails,statistics,status')->json();


            $interval = new DateInterval($response['items'][0]['contentDetails']['duration']);
            $seconds = $interval->days * 86400 + $interval->h * 3600 + $interval->i * 60 + $interval->s;

            $title = $response['items'][0]['snippet']['title'];
            $about = $response['items'][0]['snippet']['description'];
            $thumbnail = $response['items'][0]['snippet']['thumbnails']['high']['url'];
            $duration = $seconds;
        } 

        if ($request->video_type == 'upload') {
            
            $file = $request->file('pathUpload');
            $extension = $file->getClientOriginalExtension();
            $nameFinalVideo = 'video/'.Str::random(40). '.'.$extension;
            $destinatioPath = storage_path('app').'/video/';

            $getID3 = new \getID3;
            $file2 = $getID3->analyze($file);
            $playtime_seconds = $file2['playtime_seconds']; 
            $duration = $playtime_seconds;

            $path = $nameFinalVideo;
            $file->move($destinatioPath, $nameFinalVideo);
            $title = $request->title;
            $about = $request->about;

            if ($request->file('thumbnail')) {
                $thumbnailVideo= Image::make($request->file('thumbnail')->getRealPath());
                $extension = $request->file('thumbnail')->getClientOriginalExtension();
                $thumb = Image::make($thumbnailVideo)->fit(480, 360)->encode($extension, 75);
                $thumbnail = 'video_thumbnail/' . Str::random(40). '.'.$extension;
                
                Storage::put($thumbnail, $thumb->__toString());
            }
        }

        $slug = Str::slug($title);
        $created_by = $request->video_for;

        $data = array_merge(
                $request->only('privacy_setting', 'video_type'),
                compact('path', 'slug', 'duration', 'thumbnail', 'created_by', 'title', 'about')
        );

        $video = Video::create($data);

        $video->categories()->attach($categories);

        session()->flash('message', 'Video berhasil ditambahkan');
        return redirect()->route('admin.video.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        return view('admin.video.show', [
            'video' => $video
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( Video $video)
    {
        return view('admin.video.edit', [
            'video' => $video,
            'categories' => Category::orderBy('name', 'asc')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        $rules = [
            
            'privacy_setting' => 'required'
        ];

        if ($request->video_type == 'Youtube') {
            if($request->pathYoutube) {
                $rules += [
                    'pathYoutube' => [
                        'required', 
                        "regex: /^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/"
                    ],
                ];
            }
                 
        } 

        if ($request->video_type == 'Upload') {
            $rules += [
                'title' => 'required',
                'thumbnail' => 'image|mimes:jpeg,png,jpg|max:2048|dimensions:width=480,height=360',
                'about' => 'required',
                'pathUpload' => 'mimes:mp4'
            ]; 
        } 

        $request->validate($rules);

        $path = $video->path ?? null;
        $title = $video->title ?? null;
        $duration = $video->duration ?? null;
        $about = $video->about ?? null;
        $thumbnail = $video->thumbnail ?? null;
        $created_by = $video->created_by ?? null;

        if ($request->video_type == 'Youtube') {
            if($request->pathYoutube) {
                $regex = "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/";

                $url = $request->pathYoutube;
                preg_match($regex, $url, $matches);
                $videoId = $matches[1]; 
                $path = $videoId;

                $youtubeKey = config('services.youtube.key');
                $response = Http::get('https://www.googleapis.com/youtube/v3/videos?id='.$videoId.'&key='.$youtubeKey.'&part=snippet,contentDetails,statistics,status')->json();

                $interval = new DateInterval($response['items'][0]['contentDetails']['duration']);
                $seconds = $interval->days * 86400 + $interval->h * 3600 + $interval->i * 60 + $interval->s;

                $title = $response['items'][0]['snippet']['title'];
                $about = $response['items'][0]['snippet']['description'];
                $thumbnail = $response['items'][0]['snippet']['thumbnails']['high']['url'];
                $duration = $seconds;
            }
        } 

        if ($request->file('pathUpload')) {
            
            $file = $request->file('pathUpload');
            $extension = $file->getClientOriginalExtension();
            $nameFinalVideo = 'video/'.Str::random(40). '.'.$extension;
            $destinatioPath = storage_path('app').'/video/';

            $getID3 = new \getID3;
            $file2 = $getID3->analyze($file);
            $playtime_seconds = $file2['playtime_seconds']; 
            $duration = $playtime_seconds;

            if( $video->path && Storage::exists($video->path) ) {
                Storage::delete($video->path);
            }

            $path = $nameFinalVideo;
            $file->move($destinatioPath, $nameFinalVideo);

            
        }

        if ($request->file('thumbnail')) {


            $thumbnailVideo= Image::make($request->file('thumbnail')->getRealPath());

            $extension = $request->file('thumbnail')->getClientOriginalExtension();

            $thumb = Image::make($thumbnailVideo)->fit(480, 360)->encode($extension, 75);

            $thumbnail= 'video_thumbnail/' . Str::random(40). '.'.$extension;
           
            if( $video->thumbnail && Storage::exists($video->thumbnail) ) {
                 Storage::delete($video->thumbnail);
            }

            Storage::put($thumbnail, $thumb->__toString());

        } 

        $slug = Str::slug($request->title);

        $data = array_merge(
                $request->only('privacy_setting', 'video_type'),
                compact('path', 'slug', 'duration', 'thumbnail', 'created_by', 'title', 'about')
        );

        $video->update($data);

        $video->categories()->sync($request->categories);

        session()->flash('message', 'Video berhasil diupdate');
        return redirect()->route('admin.video.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        if( $video->thumbnail && Storage::exists($video->thumbnail) ) {
             Storage::delete($video->thumbnail);
        }

        if( $video->path && Storage::exists($video->path) ) {
             Storage::delete($video->path);
        }
        
        $video->delete();

        return response()->json(['success' => true]);
    }

    public function createFromChannel()
    {

        $users = User::select('id', 'name')
                    ->whereHas('channel')
                    ->get();

        session()->forget('video_from_channel');

        return view('admin.video.create_from_channel',[
            'users' => $users,
            'categories' => Category::orderBy('name', 'asc')->get()
        ]);
    }

    public function videoFromChannelIndex()
    {
        $my_videos = session()->get('video_from_channel');
        return view('admin.video.video_from_channel', [
            'my_videos' => $my_videos,
        ]);
    }

    public function videoFromChannel(Request $request)
    {
        $rules = [
                    'privacy_setting' => 'required',
                    'video_for' => 'required',
                    'youtubeChannel' => [
                        'required', 
                        "regex: /(?:https|http)\:\/\/(?:[\w]+\.)?youtube\.com\/(?:c\/|channel\/|user\/)?([a-zA-Z0-9\-]{1,})/"
                    ],
                ];

        $request->validate($rules);

        $regex = "/(?:https|http)\:\/\/(?:[\w]+\.)?youtube\.com\/(?:c\/|channel\/|user\/)?([a-zA-Z0-9\-]{1,})/";

        $url = $request->youtubeChannel;
        preg_match($regex, $url, $matches);
        $channelId = $matches[1];
        $resultsNumber = 10; 

        $API_URL = 'https://www.googleapis.com/youtube/v3/';
        $youtubeKey = config('services.youtube.key');
        $parameter = [
            'id'=> $channelId,
            'part'=> 'contentDetails',
            'key'=> $youtubeKey
        ];

        $response = Http::get($API_URL. 'channels?' . http_build_query($parameter))->json();

        if (!empty($response['items'])) {
            $playlist = $response['items'][0]['contentDetails']['relatedPlaylists']['uploads'];

            $parameter = [
                'part'=> 'snippet',
                'playlistId' => $playlist,
                'maxResults'=> $resultsNumber,
                'key'=> $youtubeKey
            ];

            $response = Http::get($API_URL. 'playlistItems?' . http_build_query($parameter))->json();

            session()->forget('video_from_channel');

            $dataSupport = [
                'channelId' => $channelId,
                'owner' => $request->video_for,
                'categories' => Category::find($request->categories),
                'privacy_setting' => $request->privacy_setting,
                'nextPageToken' => $response['nextPageToken'],
                'items' => []
            ];

            $my_videos = session()->get('video_from_channel', $dataSupport);

            foreach($response['items'] as $video){
                $my_videos['items'][] = array( 
                                    'v_id' => $video['snippet']['resourceId']['videoId'], 
                                    'v_title' => $video['snippet']['title'],
                                    'v_thumbnail' => $video['snippet']['thumbnails']['high']['url']
                                );

                session()->put('video_from_channel', $my_videos);
            }

            return redirect()->route('admin.video.video-from-channel.index');

        } else {
            session()->flash('message', 'Channel tidak mendukung untuk berbagi data.');
            return redirect()->route('admin.video.from-channel');
        }

    }

    public function moreVideoFromChannel(Request $request)
    {
        $channelId = $request->channelId;
        $token = $request->nextTokenPage;
        $resultsNumber = 10; 

        $API_URL = 'https://www.googleapis.com/youtube/v3/';
        $youtubeKey = config('services.youtube.key');

        $parameter = [
            'id'=> $channelId,
            'part'=> 'contentDetails',
            'key'=> $youtubeKey
        ];
        $channel_URL = $API_URL. 'channels?' . http_build_query($parameter);

        $response = Http::get($channel_URL)->json();

        $playlist = $response['items'][0]['contentDetails']['relatedPlaylists']['uploads'];

        $parameter = [
            'part'=> 'snippet',
            'playlistId' => $playlist,
            'maxResults'=> $resultsNumber,
            'key'=> $youtubeKey
        ];
        $channel_URL = $API_URL. 'playlistItems?' . http_build_query($parameter);

        $response = Http::get($channel_URL . '&pageToken=' . $token)->json();

        $my_videos = session()->get('video_from_channel');

        $my_videos['nextPageToken'] = $response['nextPageToken'];

        foreach($response['items'] as $video){
            $my_videos['items'][] = array( 
                                'v_id' => $video['snippet']['resourceId']['videoId'], 
                                'v_title' => $video['snippet']['title'],
                                'v_thumbnail' => $video['snippet']['thumbnails']['high']['url']
                            );

            session()->put('video_from_channel', $my_videos);
        }

        return redirect()->route('admin.video.video-from-channel.index');

    }

    public function storeVideoFromChannel(Request $request)
    {
        $rules =[
            'videoId' => 'required',
        ];

        $request->validate($rules);

        $my_videos = session()->get('video_from_channel');

        foreach ($request->videoId as $item) {
            $videoId = $item; 
            $path = $videoId;

            $youtubeKey = config('services.youtube.key');
            $response = Http::get('https://www.googleapis.com/youtube/v3/videos?id='.$videoId.'&key='.$youtubeKey.'&part=snippet,contentDetails,statistics,status')->json();

            $interval = new DateInterval($response['items'][0]['contentDetails']['duration']);
            $seconds = $interval->days * 86400 + $interval->h * 3600 + $interval->i * 60 + $interval->s;

            $title = $response['items'][0]['snippet']['title'];
            $about = $response['items'][0]['snippet']['description'];
            $thumbnail = $response['items'][0]['snippet']['thumbnails']['high']['url'];
            $duration = $seconds;
            $slug = Str::slug($title);
            $created_by = $my_videos['owner'];
            $privacy_setting = $my_videos['privacy_setting'];
            $video_type= "youtube";


            $data = compact('privacy_setting', 'video_type', 'path', 'slug', 'duration', 'thumbnail', 'created_by', 'title', 'about');

            $video = Video::create($data);

            $video->categories()->attach($my_videos['categories']);
        }

        session()->forget('video_from_channel');
        session()->flash('message', 'Video berhasil ditambahkan');

        return redirect()->route('admin.video.index');
            
    }

    public function searchVideoFromChannel(Request $request)
    {
        $rules =[
            'keyword' => 'required',
        ];

        $request->validate($rules);

        $my_videos = session()->get('video_from_channel');

        $channelId = $request->channelId;
        $keyword = $request->keyword ? $request->keyword : '';
        $API_URL = 'https://www.googleapis.com/youtube/v3/';
        $youtubeKey = config('services.youtube.key');
        $resultsNumber = 10; 

        $parameter = [
            'part'=> 'snippet',
            'channelId' => $channelId,
            'order' => 'relevance',
            'q' => $keyword,
            'maxResults'=> $resultsNumber,
            'key' => $youtubeKey
        ];

        $channel_URL = $API_URL. 'search?' . http_build_query($parameter);
        $response = Http::get($channel_URL)->json();
        // dd($response);

        session()->forget('search_video_from_channel');

        if (!empty($response['items'])) {
            
            $dataSupport = [
                'channelId' => $channelId,
                'owner' => $my_videos['owner'],
                'privacy_setting' => $my_videos['privacy_setting'],
                'nextPageToken' => !empty($response['nextPageToken']) ? $response['nextPageToken'] : NULL,
                'keyword' => $request->keyword,
                'items' => []
            ];

            $search_videos = session()->get('search_video_from_channel', $dataSupport);

            foreach($response['items'] as $video){
                $search_videos['items'][] = array( 
                                    'v_id' => $video['id']['videoId'], 
                                    'v_title' => $video['snippet']['title'],
                                    'v_thumbnail' => $video['snippet']['thumbnails']['high']['url']
                                );

                session()->put('search_video_from_channel', $search_videos);
            }

            return redirect()->route('admin.video.search.video-from-channel.index');

        } else {
            return redirect()->route('admin.video.search.video-from-channel.index');
        }
            
    }

    public function searchVideoFromChannelIndex()
    {
        $search_videos = session()->get('search_video_from_channel');
        $my_videos = session()->get('video_from_channel');
        return view('admin.video.video_from_channel_search', [
            'search_videos' => $search_videos,
            'my_videos' => $my_videos,
        ]);
    }

    public function searchMoreVideoFromChannel(Request $request)
    {
        $search_videos = session()->get('search_video_from_channel');

        $channelId = $request->channelId;
        $token = $request->nextTokenPage;
        $resultsNumber = 10; 

        $API_URL = 'https://www.googleapis.com/youtube/v3/';
        $youtubeKey = config('services.youtube.key');

        $parameter = [
            'part'=> 'snippet',
            'channelId' => $channelId,
            'order' => 'relevance',
            'q' => $search_videos['keyword'],
            'maxResults'=> $resultsNumber,
            'key' => $youtubeKey
        ];

        $channel_URL = $API_URL. 'search?' . http_build_query($parameter);

        $response = Http::get($channel_URL . '&pageToken=' . $token)->json();

        $search_videos['nextPageToken'] = $response['nextPageToken'];

        foreach($response['items'] as $video){
            $search_videos['items'][] = array( 
                                'v_id' => $video['id']['videoId'], 
                                'v_title' => $video['snippet']['title'],
                                'v_thumbnail' => $video['snippet']['thumbnails']['high']['url']
                            );

            session()->put('search_video_from_channel', $search_videos);
        }

        return redirect()->route('admin.video.search.video-from-channel.index');
    }

}
