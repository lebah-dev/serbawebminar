<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('admin.guest')->except('logout');
    }

    public function broker()
    {
        return Password::broker('admins');
    }


    public function index()
    {
    	return view('admin.auth.passwords.email');
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }
}
