<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Mail\Admin\AdminStoreConfirmation;
use App\Models\Admin;
use Mail;

class LoginController extends Controller
{
	use AuthenticatesUsers;
	

	public function __construct()
	{
		$this->middleware('admin.guest')->except('logout');
	}

    public function index(){
    	return view('admin.auth.login');
    }

    protected function guard()
    {
    	return auth()->guard('admin');
    }

    public function login(Request $request)
    {
    	$request->validate([
    		'email' => 'required|email',
    		'password' => 'required',
    	]);
    	
    	$admin = Admin::where('email', $request->email)->first();

    	if ($admin) {
    		if ($admin->email_verified_at) {
                if (Hash::check($request->password, $admin->password)) {
                    if ($this->guard()->attempt([
                        'email' => $request->email,
                        'password' => $request->password,
                    ])) {
                        session()->flash('message', 'Selamat datang kembali di halaman admin.');
                        return redirect()->route('admin.dashboard.index');
                    } else {
                        session()->flash('message', 'Email atau password salah');
                        return redirect()->back();
                    }
                } else {
                    session()->flash('message', 'Email atau password salah');
                    return redirect()->back();
                }
            } else {
                session()->flash('message_verified', 'Sebelum melanjutkan, periksa email Anda untuk verifikasi. Jika tidak menerima email, <a href="'.route('admin.resend-verifyemail', $admin->email).'" class="alert-link"> klik di sini untuk meminta lagi</a>');
                return redirect()->back();
            }

    	} else {
    		session()->flash('message', 'Email atau password salah');
    		return redirect()->back();
    	}
    }

    public function resendVerifyEmail($email)
    {
        $admin = Admin::where('email', $email)->firstOrFail();

        Mail::to($admin->email)->send(new AdminStoreConfirmation($admin));

        session()->flash('message_verified', 'Berhasil meminta email kembali');
        return redirect()->back();
    }

    public function logout(Request $request)
    {
    	auth()->guard('admin')->logout();

    	session()->flash('message', 'Berhasil keluar dari sistem');

    	return redirect()->route('welcome');
    }
}
