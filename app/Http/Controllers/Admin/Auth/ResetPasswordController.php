<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Auth;

class ResetPasswordController extends Controller
{
	use ResetsPasswords;

	protected $redirectTo = '/admin/dashboard';

    public function __construct()
    {
        $this->middleware('admin.guest')->except('logout');
    }

    public function broker()
    {
        return Password::broker('admins');
    }

    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => [
                        'required', 'min:8', 'max:40', 'string', 'confirmed',
                        'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
                    ],
        ];
    }

    public function validationErrorMessages()
    {
        return [
                    'email.required' => 'Email Dibutuhkan!',
                    'email.email' => 'Email harus alamat email yang valid.',
                    'password.required' => 'Password Dibutuhkan!',
                    'password.string' => 'Password harus string!',
                    'password.min' => 'Kata sandi minimal 8 karakter!',
                    'password.max' => 'Kata sandi maximal 40 karakter!',
                    'password.confirmed' => 'Konfirmasi Password Tidak Cocok!',
                    'password.regex' => 'Minimal 8 characters, termasuk 1 huruf kapital, 1 angka.',
        ];
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('admin.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }
}
