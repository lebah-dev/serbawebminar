<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{ Comment, User, Video };

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.comments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::select('id', 'name')->get();
        $videos = Video::select('id', 'title')->get();

        return view('admin.comments.create', [
            'users' => $users,
            'videos' => $videos
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'user_id' => 'required',
            'video_id' => 'required',
            'content' => 'required',
        ];

        $request->validate($rules, [
            'user_id.required' => 'Nama Pengirim Dibutuhkan!',
            'video_id.required' => 'Video Dikomentari Dibutuhkan!',
            'content.required' => 'Komentar Dibutuhkan!',
        ]);


        $data = $request->except('_token', '_method');

        Comment::create($data);

        session()->flash('message', 'Comment baru berhasil ditambahkan');
        return redirect()->route('admin.comments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return view('admin.comments.show', [
            'comment' => $comment
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {

        return view('admin.comments.edit', [
            'comment' => $comment,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        $rules = [
            'content' => 'required',
        ];

        $request->validate($rules, [
            'content.required' => 'Komentar Dibutuhkan!',
        ]);

        $data = $request->only('content');

        $comment->update($data);

        session()->flash('message', 'Comment berhasil diupdate');
        return redirect()->route('admin.video.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        session()->flash('message', 'Comment berhasil dihapus');
        return redirect()->route('admin.video.index');
    }

    public function destroyReply($id)
    {
        $comment = Comment::findOrFail($id);

        $comment->delete();

        session()->flash('message', 'Reply comment berhasil dihapus');
        return redirect()->route('admin.comments.index');
    }
}
