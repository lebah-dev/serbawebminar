<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{ Video, User, Channel, Comment };

class DashboardController extends Controller
{
    public function index()
    {
    	$totalVideos = Video::count();
    	$totalChannels = Channel::count();
    	$totalUsers = User::count();
    	$totalComments = Comment::count();

    	return view('admin.dashboard.index', [
    		'totalVideos' => $totalVideos,
    		'totalChannels' => $totalChannels,
    		'totalUsers' => $totalUsers,
    		'totalComments' => $totalComments
    	]);
    }
}
