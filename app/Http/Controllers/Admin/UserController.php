<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{ File, Hash, Storage, DB, Mail};
use App\Models\User;
use App\Mail\Admin\UserStoreConfirmation;
use Exception;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = null;

        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => [
                'required', 'min:8', 'max:40', 'string', 'confirmed',
                'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
            ],
            'password_confirmation' => 'required'
        ];

        if ($request->avatar) {
            $rules += [
                'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048'
            ]; 
        } 

        $request->validate($rules, [
            'name.required' => 'Nama Dibutuhkan!',
            'email.required' => 'Email Dibutuhkan!',
            'email.email' => 'Email harus alamat email yang valid.',
            'email.unique' => 'Email sudah digunakan.',
            'avatar.required' => 'Avatar Dibutuhkan!',
            'avatar.image' => 'Avatar harus berupa gambar.',
            'avatar.mimes' => 'Avatar hanya mendukung ektensi: .jpg, .jpeg, .png',
            'password.required' => 'Password Dibutuhkan!',
            'password.string' => 'Password harus string!',
            'password.min' => 'Kata sandi minimal 8 karakter!',
            'password.max' => 'Kata sandi maximal 40 karakter!',
            'password.confirmed' => 'Konfirmasi Password Tidak Cocok!',
            'password.regex' => 'Minimal 8 characters, termasuk 1 huruf kapital, 1 angka.',
            'password_confirmation.required' => 'Konfirmasi Password Dibutuhkan!',
        ]);

        DB::beginTransaction();

        try {
            $password = Hash::make($request->password);

            $data = array_merge(
                $request->only('name', 'email'),
                compact('password')
            );

            $user = User::create($data);

           

            if ($request->file('avatar')) {

                $image = Image::make($request->file('avatar')->getRealPath());
                $extension = $request->file('avatar')->getClientOriginalExtension();
                $thumb = Image::make($image)->fit(60)->encode($extension, 75);
                $origin = Image::make($image)->fit(1024)->encode($extension, 75);
                $thumbPath = 'avatar-thumb/' . Str::random(40). '.'.$extension;
                $mainPath = 'avatar/' . Str::random(40). '.'.$extension;

                Storage::put($mainPath, $origin->__toString());
                Storage::put($thumbPath, $thumb->__toString());

                $existsAvatar = $user->user_avatar()->count();

                if ($existsAvatar === 0) {
                    $user->user_avatar()->create([
                        'thumbnail' => $thumbPath,
                        'main_path' => $mainPath
                    ]);
                }
            }

            Mail::to($user->email)->send(new UserStoreConfirmation($user));

            DB::commit();

            session()->flash('success', 'User berhasil ditambahkan');
            return redirect()->route('admin.users.index');
            
        } catch (Exception $e) {
            DB::rollback();

            session()->flash('error', 'User tidak berhasil ditambahkan');

            return redirect()->route('admin.users.create');
        }

                
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.users.show', [
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', [
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
        ];


        if ($request->avatar) {
            $rules += [
                'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048'
            ]; 
        } 

        if ($request->password) {
            $rules += [
                    'password' => [
                    'required', 'min:8', 'max:40', 'string', 'confirmed',
                    'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
                ],
                'password_confirmation' => 'required'
            ];  
        }

        $request->validate($rules, [
            'name.required' => 'Nama Dibutuhkan!',
            'email.required' => 'Email Dibutuhkan!',
            'email.email' => 'Email harus alamat email yang valid.',
            'email.unique' => 'Email sudah digunakan.',
            'avatar.required' => 'Avatar Dibutuhkan!',
            'avatar.image' => 'Avatar harus berupa gambar.',
            'avatar.mimes' => 'Avatar hanya mendukung ektensi: .jpg, .jpeg, .png',
            'password.required' => 'Password Dibutuhkan!',
            'password.string' => 'Password harus string!',
            'password.min' => 'Kata sandi minimal 8 karakter!',
            'password.max' => 'Kata sandi maximal 40 karakter!',
            'password.confirmed' => 'Konfirmasi Password Tidak Cocok!',
            'password.regex' => 'Minimal 8 characters, termasuk 1 huruf kapital, 1 angka.',
            'password_confirmation.required' => 'Konfirmasi Password Dibutuhkan!',
        ]);

        $password = Hash::make($request->password);

        $data = array_merge(
            $request->only('name', 'email'),
            compact('password')
        );

        $user->update($data);

        if ($request->file('avatar')) {

            $image = Image::make($request->file('avatar')->getRealPath());

            $extension = $request->file('avatar')->getClientOriginalExtension();

            $thumb = Image::make($image)->fit(60)->encode($extension, 75);
            $origin = Image::make($image)->fit(1024)->encode($extension, 75);
            $thumbPath = 'avatar-thumb/' . Str::random(40). '.'.$extension;
            $mainPath = 'avatar/' . Str::random(40). '.'.$extension;

            Storage::put($mainPath, $origin->__toString());
            Storage::put($thumbPath, $thumb->__toString());

            $existsAvatar = $user->user_avatar()->count() ? true : false;

            if ($existsAvatar) {

                if( $user->user_avatar->thumbnail && Storage::exists($user->user_avatar->thumbnail)) {
                    Storage::delete($user->user_avatar->thumbnail);
                }

                if( $user->user_avatar->main_path && Storage::exists($user->user_avatar->main_path)) {
                    Storage::delete($user->user_avatar->main_path);
                }

                $user->user_avatar()->update([
                    'thumbnail' => $thumbPath,
                    'main_path' => $mainPath
                ]);
            } else {
                $user->user_avatar()->create([
                    'thumbnail' => $thumbPath,
                    'main_path' => $mainPath
                ]);
            }
        }

        session()->flash('message', 'User berhasil diupdate');
        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $existsAvatar = $user->user_avatar()->count() ? true : false;

        if ($existsAvatar) {
            if( $user->user_avatar->thumbnail && Storage::exists($user->user_avatar->thumbnail)) {
                Storage::delete($user->user_avatar->thumbnail);
            }

            if( $user->user_avatar->main_path && Storage::exists($user->user_avatar->main_path)) {
                Storage::delete($user->user_avatar->main_path);
            }
        }
        
        $user->delete();

        return response()->json(['success' => true]);
    }
}
