<?php

namespace App\Http\Controllers\Admin\DataTable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $comments = Comment::whereNull('parent_id')->orderBy('created_at', 'asc');

        return datatables()->of($comments)
                        ->addColumn('action', 'layouts.admin.DT-button.DT-comments-action')
                        ->rawColumns(['action'])
                        ->addIndexColumn()
                        ->toJson();
    }
}
