<?php

namespace App\Http\Controllers\Admin\DataTable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;

class AdminController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $users = Admin::orderBy('created_at', 'asc');

        return datatables()->of($users)
                        ->addColumn('action', 'layouts.admin.DT-button.DT-admins-action')
                        ->rawColumns(['action'])
                        ->addIndexColumn()
                        ->toJson();
    }
}
