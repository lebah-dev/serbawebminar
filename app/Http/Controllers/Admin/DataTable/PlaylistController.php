<?php

namespace App\Http\Controllers\Admin\DataTable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Playlist;

class PlaylistController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $playliss = Playlist::orderBy('created_at', 'asc');

        return datatables()->of($playliss)
                        ->addColumn('action', 'layouts.admin.DT-button.DT-playlist-action')
                        ->rawColumns(['action'])
                        ->addIndexColumn()
                        ->toJson();
    }
}
