<?php

namespace App\Http\Controllers\Admin\DataTable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;

class BannerController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $banners = Banner::orderBy('name', 'asc');

        return datatables()->of($banners)
                        ->editColumn('image', 'layouts.admin.DT-button.DT-banners-image')
                        ->editColumn('status',function (Banner $model){
                            return $model->status === 'on' ? '<span class="label label-inline label-light-success font-weight-bold">
                                Digunakan
                            </span>' : '';
                        })
                        ->addColumn('action', 'layouts.admin.DT-button.DT-banners-action')
                        ->rawColumns(['action', 'image', 'status'])
                        ->addIndexColumn()
                        ->toJson();
    }
}
