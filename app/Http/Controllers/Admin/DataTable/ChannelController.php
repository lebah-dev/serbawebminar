<?php

namespace App\Http\Controllers\Admin\DataTable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Channel;

class ChannelController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $videos = Channel::orderBy('created_at', 'asc');

        return datatables()->of($videos)
                        ->editColumn('cover_image', 'layouts.admin.DT-button.DT-channel-image')
                        ->addColumn('action', 'layouts.admin.DT-button.DT-channel-action')
                        ->rawColumns(['action', 'cover_image'])
                        ->addIndexColumn()
                        ->toJson();
    }
}
