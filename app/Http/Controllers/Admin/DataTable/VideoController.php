<?php

namespace App\Http\Controllers\Admin\DataTable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Video;

class VideoController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $videos = Video::orderBy('created_at', 'asc');

        return datatables()->of($videos)
                        ->editColumn('thumbnail', 'layouts.admin.DT-button.DT-video-image')
                        ->addColumn('action', 'layouts.admin.DT-button.DT-video-action')
                        ->rawColumns(['action', 'thumbnail'])
                        ->addIndexColumn()
                        ->toJson();
    }
}
