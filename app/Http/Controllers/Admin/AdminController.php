<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{ File, Hash, Storage, DB, Mail };
use App\Models\Admin;
use App\Mail\Admin\AdminStoreConfirmation;
use Exception;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.admins.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = null;

        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:admins',
            'password' => [
                'required', 'min:8', 'max:40', 'string', 'confirmed',
                'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
            ],
            'password_confirmation' => 'required'
        ];

        if ($request->avatar) {
            $rules += [
                'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048'
            ]; 
        } 

        $request->validate($rules, [
            'name.required' => 'Nama Dibutuhkan!',
            'email.required' => 'Email Dibutuhkan!',
            'email.email' => 'Email harus alamat email yang valid.',
            'email.unique' => 'Email sudah digunakan.',
            'avatar.required' => 'Avatar Dibutuhkan!',
            'avatar.image' => 'Avatar harus berupa gambar.',
            'avatar.mimes' => 'Avatar hanya mendukung ektensi: .jpg, .jpeg, .png',
            'password.required' => 'Password Dibutuhkan!',
            'password.string' => 'Password harus string!',
            'password.min' => 'Kata sandi minimal 8 karakter!',
            'password.max' => 'Kata sandi maximal 40 karakter!',
            'password.confirmed' => 'Konfirmasi Password Tidak Cocok!',
            'password.regex' => 'Minimal 8 characters, termasuk 1 huruf kapital, 1 angka.',
            'password_confirmation.required' => 'Konfirmasi Password Dibutuhkan!',
        ]);

        DB::beginTransaction();

        try {

                

                $password = Hash::make($request->password);

                $data = array_merge(
                    $request->only('name', 'email'),
                    compact('password')
                );

                $admin = Admin::create($data);

                

                if ($request->file('avatar')) {

                    $image = Image::make($request->file('avatar')->getRealPath());

                    $extension = $request->file('avatar')->getClientOriginalExtension();

                    $thumb = Image::make($image)->fit(60)->encode($extension, 75);
                    $origin = Image::make($image)->fit(1024)->encode($extension, 75);
                    $pathThumb = 'adminAvatar-thumb/' . Str::random(40). '.'.$extension;
                    $path = 'adminAvatar/' . Str::random(40). '.'.$extension;

                    Storage::put($path, $origin->__toString());
                    Storage::put($pathThumb, $thumb->__toString());

                    $existsAvatar = $admin->admin_avatar()->count();

                    if ($existsAvatar === 0) {
                        $admin->admin_avatar()->create([
                            'thumbnail' => $pathThumb,
                            'main_path' => $path
                        ]);
                    }
                }

                Mail::to($admin->email)->send(new AdminStoreConfirmation($admin));
                DB::commit();

                session()->flash('success', 'Admin berhasil ditambahkan');
                return redirect()->route('admin.admins.index');
        } catch (Exception $e) {
            DB::rollback();

            session()->flash('error', 'Admin tidak berhasil ditambahkan');

            return redirect()->route('admin.admins.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        return view('admin.admins.show', [
             'admin' => $admin
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        return view('admin.admins.edit', [
            'admin' => $admin
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$admin->id,
        ];


        if ($request->avatar) {
            $rules += [
                'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048'
            ]; 
        } 

        if ($request->password) {
            $rules += [
                    'password' => [
                    'required', 'min:8', 'max:40', 'string', 'confirmed',
                    'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
                ],
                'password_confirmation' => 'required'
            ];  
        }

        $request->validate($rules, [
            'name.required' => 'Nama Dibutuhkan!',
            'email.required' => 'Email Dibutuhkan!',
            'email.email' => 'Email harus alamat email yang valid.',
            'email.unique' => 'Email sudah digunakan.',
            'avatar.required' => 'Avatar Dibutuhkan!',
            'avatar.image' => 'Avatar harus berupa gambar.',
            'avatar.mimes' => 'Avatar hanya mendukung ektensi: .jpg, .jpeg, .png',
            'password.required' => 'Password Dibutuhkan!',
            'password.string' => 'Password harus string!',
            'password.min' => 'Kata sandi minimal 8 karakter!',
            'password.max' => 'Kata sandi maximal 40 karakter!',
            'password.confirmed' => 'Konfirmasi Password Tidak Cocok!',
            'password.regex' => 'Minimal 8 characters, termasuk 1 huruf kapital, 1 angka.',
            'password_confirmation.required' => 'Konfirmasi Password Dibutuhkan!',
        ]);

        $password = Hash::make($request->password);

        $data = array_merge(
            $request->only('name', 'email'),
            compact('password')
        );

        $admin->update($data);

        if ($request->file('avatar')) {

            $image = Image::make($request->file('avatar')->getRealPath());

            $extension = $request->file('avatar')->getClientOriginalExtension();

            $thumb = Image::make($image)->fit(60)->encode($extension, 75);
            $origin = Image::make($image)->fit(1024)->encode($extension, 75);
            $thumbPath = 'adminAvatar-thumb/' . Str::random(40). '.'.$extension;
            $path = 'adminAvatar/' . Str::random(40). '.'.$extension;

            Storage::put($path, $origin->__toString());
            Storage::put($thumbPath, $thumb->__toString());

            $existsAvatar = $admin->admin_avatar()->count();

            if ($existsAvatar) {
                if( $admin->admin_avatar->thumbnail && Storage::exists($admin->admin_avatar->thumbnail) ) {
                    Storage::delete($admin->admin_avatar->thumbnail);
                }

                if( $admin->admin_avatar->main_path && Storage::exists($admin->admin_avatar->main_path) ) {
                    Storage::delete($admin->admin_avatar->main_path);
                }

                $admin->admin_avatar()->update([
                    'thumbnail' => $thumbPath,
                    'main_path' => $path
                ]);
            } else {
                $admin->admin_avatar()->create([
                    'thumbnail' => $thumbPath,
                    'main_path' => $path
                ]);
            }
        }

        session()->flash('message', 'Admin berhasil diupdate');
        return redirect()->route('admin.admins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        if ($admin->admin_avatar()->count() === 1) {
            if( $admin->admin_avatar->thumbnail && Storage::exists($admin->admin_avatar->thumbnail) ) {
                Storage::delete($admin->admin_avatar->thumbnail);
            }

            if( $admin->admin_avatar->main_path && Storage::exists($admin->admin_avatar->main_path) ) {
                Storage::delete($admin->admin_avatar->main_path);
            }
        }
        
        $admin->delete();

        return response()->json(['success' => true]);
    }
}
