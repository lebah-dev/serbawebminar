<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{ Channel, Playlist };

class PlaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.playlist.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $channels = Channel::select('id', 'name')->get();

        return view('admin.playlist.create', [
            'channels' => $channels
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:playlists',
            'channel_id' => 'required',
            'privacy_setting' => 'required',
        ];

        $request->validate($rules, [
            'name.required' => 'Nama Dibutuhkan!',
            'name.unique' => 'Nama Sudah Digunakan!',
            'channel_id.required' => 'Channel Dibutuhkan!',
            'privacy_setting.required' => 'Privacy Setting Dibutuhkan!'
        ]);

        $data = $request->except('_token', '_method');

        Playlist::create($data);

        session()->flash('message', 'Playlist berhasil ditambahkan');
        return redirect()->route('admin.playlist.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Playlist $playlist)
    {
        return view('admin.playlist.show', [
            'playlist' => $playlist
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Playlist $playlist)
    {
        return view('admin.playlist.edit', [
            'playlist' => $playlist
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Playlist $playlist)
    {
        $rules = [
            'name' => 'required|unique:playlists,name,'.$playlist->id,
            'channel_id' => 'required',
            'privacy_setting' => 'required',
        ];

        $request->validate($rules, [
            'name.required' => 'Nama Dibutuhkan!',
            'name.unique' => 'Nama Sudah Digunakan!',
            'channel_id.required' => 'Channel Dibutuhkan!',
            'privacy_setting.required' => 'Privacy Setting Dibutuhkan!'
        ]);

        $data = $request->except('_token', '_method');

        $playlist->update($data);

        session()->flash('message', 'Playlist berhasil diupdate');
        return redirect()->route('admin.playlist.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Playlist $playlist)
    {
        $playlist->delete();

        return response()->json(['success' => true]);
    }
}
