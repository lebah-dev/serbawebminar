<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);
        
        $image = null;

        if ($request->hasFile('image')) {
               $image = $request->file('image')->store('categories', 'public');
        }

        $slug = Str::slug($request->name);

        $data = array_merge(
            $request->only('name'),
            compact('image', 'slug')
        );

        Category::create($data);

        session()->flash('message', 'Data sukses ditambahkan');

        return redirect()->route('admin.category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {

        return view('admin.category.edit', [
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {

        if ($request->image) {
            $request->validate([
                'name' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
            ]);
        }

        $request->validate([
            'name' => 'required',
        ]);

        $image = $category->image ?? null;

        if ($request->hasFile('image')) {
            if( $category->image && file_exists(storage_path('app/public/'.$category->image)) ) {
                File::delete(storage_path('app/public/'.$category->image));
            }

            $image = $request->file('image')->store('categories', 'public');
        }

        $slug = Str::slug($request->name);


        $data = array_merge(
            $request->only('name'),
            compact('image', 'slug')
        );


        $category->update($data);

        session()->flash('message', 'Data sukses diupdate');

        return redirect()->route('admin.category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {

        if( $category->image && file_exists(storage_path('app/public/'.$category->image)) ) {
            File::delete(storage_path('app/public/'.$category->image));
        }
        
        $category->delete();

        return response()->json(['success' => true]);
    }
}
