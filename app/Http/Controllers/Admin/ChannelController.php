<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Channel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Crypt;


class ChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.channel.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $owner = Crypt::decrypt($request->owner);

        $users = User::select('id', 'name')
                    ->whereDoesntHave('channel')
                    ->where('id', $owner)
                    ->first();

        return view('admin.channel.create',[
            'users' => $users
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $http_request(method, url)t
     * @return \Illuminate\Http\Response
     */                                                             
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'user_id' => 'required|unique:channels',
            'business' => 'required',
            'description' => 'required',
        ];

         if ($request->cover_image) {
            $rules += [
                'cover_image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
            ]; 
        }

        $request->validate($rules, [
            'name.required' => 'Nama Dibutuhkan!',
            'user_id.required' => 'Channel For Dibutuhkan!',
            'user_id.unique' => 'User sudah memiliki channel.',
            'business.required' => 'Business Dibutuhkan!',
            'cover_image.required' => 'Image Dibutuhkan!',
            'cover_image.image' => 'Image harus berupa gambar.',
            'cover_image.mimes' => 'Image hanya mendukung ektensi: .jpg, .jpeg, .png',
            'description.required' => 'Description Dibutuhkan!'
        ]);


         $links = Str::slug($request->name);

         $cover_image = null;

        if ($request->file('cover_image')) {

            $cover_image = Image::make($request->file('cover_image')->getRealPath());
            $extension = $request->file('cover_image')->getClientOriginalExtension();

            $origin = Image::make($cover_image)->fit(1024)->encode($extension, 75);
            $path = 'channel/' . Str::random(40). '.'.$extension;

            $cover_image = $path;

            Storage::put($path, $origin->__toString());

        }

         $data = array_merge(
            $request->except('_token', 'cover_image'),
            compact('cover_image', 'links')

         );

         Channel::create($data);

        session()->flash('success', 'Channel berhasil ditambahkan');
        return redirect()->route('admin.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Channel $channel)
    {
        return view('admin.channel.show', [
            'channel' => $channel
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Channel $channel)
    {
        return view('admin.channel.edit', [
            'channel' => $channel
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Channel $channel)
    {
        $rules = [
            'name' => 'required',
            // 'user_id' => 'required|unique:channels,user_id,'.$channel->user_id,
            'business' => 'required',
            'description' => 'required',
        ];

         if ($request->cover_image) {
            $rules += [
                'cover_image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
            ]; 
        }

        $request->validate($rules, [
            'name.required' => 'Nama Dibutuhkan!',
            'user_id.required' => 'Channel For Dibutuhkan!',
            'user_id.unique' => 'User sudah memiliki channel.',
            'business.required' => 'Business Dibutuhkan!',
            'cover_image.required' => 'Image Dibutuhkan!',
            'cover_image.image' => 'Image harus berupa gambar.',
            'cover_image.mimes' => 'Image hanya mendukung ektensi: .jpg, .jpeg, .png',
            'description.required' => 'Description Dibutuhkan!'
        ]);

         $links = Str::slug($request->name);

         $cover_image = $channel->cover_image ?? null;

         if ($request->file('cover_image')) {

            $cover_image = Image::make($request->file('cover_image')->getRealPath());
            $extension = $request->file('cover_image')->getClientOriginalExtension();

            $origin = Image::make($cover_image)->fit(1024)->encode($extension, 75);
            $path = 'channel/' . Str::random(40). '.'.$extension;

            $cover_image = $path;

            if( $channel->cover_image && file_exists(storage_path('app/'.$channel->cover_image)) ) {
                Storage::delete($channel->cover_image);
            }

            Storage::put($path, $origin->__toString());

        }

        $data = array_merge(
            $request->except('_token', '_method', 'cover_image'),
            compact('cover_image', 'links')

        );

        $channel->update($data);

        session()->flash('success', 'Channel berhasil diupdate');
        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Channel $channel)
    {
        if( $channel->cover_image && file_exists(storage_path('app/'.$channel->cover_image)) ) {
            Storage::delete($channel->cover_image);
        }
        
        $channel->delete();

        // return response()->json(['success' => true]);
        session()->flash('success', 'Channel berhasil dihapus');
        return redirect()->route('admin.users.index');
    }
}
