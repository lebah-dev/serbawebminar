<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Rules\Admin\CurrentPassword;
use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class ProfilController extends Controller
{
    public function index() 
    {
    	$admin = Auth::guard('admin')->user();

    	return view('admin.profil.index', [
    		'admin' => $admin
    	]);
    }

    public function edit()
    {
    	$admin = Auth::guard('admin')->user();

    	return view('admin.profil.edit', [
    		'admin' => $admin
    	]);
    }

    public function update(Request $request, $id)
    {
    	$admin = Auth::guard('admin')->user();

    	if ($request->avatar) {
            $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:admins,email,'.$id,
                'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048'
            ]);
        }

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:admins,email,'.$id,
        ]);

	     if ($request->file('avatar')) {

	     	$image = Image::make($request->file('avatar')->getRealPath());

	     	$extension = $request->file('avatar')->getClientOriginalExtension();

	     	$thumb = Image::make($image)->fit(60)->encode($extension, 75);
	        $origin = Image::make($image)->fit(1024)->encode($extension, 75);
	        $path = 'adminAvatar/' . Str::random(40). '.'.$extension;
	        $thumbPath = 'adminAvatar-thumb/' . Str::random(40). '.'.$extension;

	        

	     	Storage::put($path, $origin->__toString());
	   	 	Storage::put($thumbPath, $thumb->__toString());

	   	 	$existsAvatar = $admin->admin_avatar()->count();

	        if ($existsAvatar) {
                if( $admin->admin_avatar->thumbnail ) {
                    Storage::delete($admin->admin_avatar->thumbnail);
                }

                if( $admin->admin_avatar->main_path) {
                    Storage::delete($admin->admin_avatar->main_path);
                }

                $admin->admin_avatar()->update([
	                'thumbnail' => $thumbPath,
	                'main_path' => $path
	            ]);
	        } else {
	            $admin->admin_avatar()->create([
	                'thumbnail' => $thumbPath,
	                'main_path' => $path
	            ]);
	        }


	   
        }

        session()->flash('message', 'Profil berhasil diupdate');
        return redirect()->route('admin.profil.index');
    }

    public function changePassword(Request $request, $id)
    {

    	$this->validate( $request, [
            'current_password' => ['required', 'string', new CurrentPassword()],
            'password' => [
                'required', 'min:8', 'max:40', 'string', 'confirmed',
                'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
            ],
            'password_confirmation' => 'required'
        ], [
        	'current_password.required' => 'Password Saat ini Dibutuhkan!',
            'password.required' => 'Password Dibutuhkan!',
            'password.string' => 'Password harus string!',
            'password.min' => 'Kata sandi minimal 8 karakter!',
            'password.max' => 'Kata sandi maximal 40 karakter!',
            'password.confirmed' => 'Konfirmasi Password Tidak Cocok!',
            'password.regex' => 'Minimal 8 characters, termasuk 1 huruf kapital, 1 angka.',
            'password_confirmation.required' => 'Konfirmasi Password Dibutuhkan!',
        ]);

    	if (Hash::check($request->current_password, Auth::guard('admin')->user()->password)) {
    	 	$data = ['password' => Hash::make($request->password)];

    	 	Admin::findOrFail($id)->update($data);

    	 	session()->flash('message', 'Password Berhasil dirubah.');
    	 	return redirect()->back(); 
    	 	
    	}
    	
    }
}
