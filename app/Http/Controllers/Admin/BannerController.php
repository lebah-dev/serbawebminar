<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use App\Models\Banner;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.banners.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->status == "on") {
            $bannerOn = Banner::where('status', 'on')->first();

            if (!empty($bannerOn)) {
                $bannerOn->update([
                    'status' => 'off'
                ]);
                $this->bannerStore($request);

                session()->flash('message', 'Banner Ads sukses ditambahkan');

                return redirect()->route('admin.banner.index');

            } else {
                $this->bannerStore($request);

                session()->flash('message', 'Banner Ads sukses ditambahkan');

                return redirect()->route('admin.banner.index');
            }

        } 
        
        $this->bannerStore($request);

        session()->flash('message', 'Banner Ads sukses ditambahkan');

        return redirect()->route('admin.banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        return view('admin.banners.edit', [
            'banner' => $banner
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        if ($request->status == 'on') {
                $bannerOn = Banner::where('status', 'on')->first();

                if (!empty($bannerOn)) {
                    $bannerOn->update([
                        'status' => 'off'
                    ]);

                    $this->bannerUpdate($request, $banner);
                    
                    session()->flash('message', 'Data sukses diupdate');

                    return redirect()->route('admin.banner.index');
                } else {
                    $this->bannerUpdate($request, $banner);
                    
                    session()->flash('message', 'Data sukses diupdate');

                    return redirect()->route('admin.banner.index');
                }
           }   

        $this->bannerUpdate($request, $banner);
        
        session()->flash('message', 'Data sukses diupdate');

        return redirect()->route('admin.banner.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        if( $banner->image && Storage::exists($banner->image) ) {
            Storage::delete($banner->image);
        }

        $banner->delete();

        return response()->json(['success' => true]);
    }

    private function bannerStore(Request $request) 
    {
        $rules = [
            'name' => 'required',
            'image' => 'required|image|max:2048',
            'description' => 'required',
        ];

        $request->validate($rules, [
            'name.required' => 'Nama Banner Ads Dibutuhkan!',
            'image.required' => 'Gambar Dibutuhkan!',
            'image.image' => 'File harus berupa gambar.',
            'image.mimes' => 'Gambar harus berupa file dengan jenis: jpeg, png, jpg.',
            'image.dimensions' => 'Dimensi gambar tidak valid.',
            'description.required' => 'Komentar Dibutuhkan!',
        ]);

        $image = null;

        if ($request->file('image')) {
                $realPath = Image::make($request->file('image')->getRealPath());
                $extension = $request->file('image')->getClientOriginalExtension();
                $origin = Image::make($realPath)->encode($extension, 75);
                $path = 'banners/' . Str::random(40).'.'.$extension;
                
                Storage::put($path, $origin->__toString());
                $image = $path;
        }

        $slug = Str::slug($request->name);
        $status = "off";

        if ($request->status == "on") {
            $status = "on";
        }

        $data = array_merge(
            $request->only('name', 'description'),
            compact('image', 'slug', 'status')
        );

        Banner::create($data);

    }

    private function bannerUpdate(Request $request, Banner $banner)
    {
        $rules = [
            'name' => 'required',
            'description' => 'required',
        ];

        if ($request->image) {
            $rules += [
                'image' => 'required|image|mimes:jpeg,png,jpg|max:2048|dimensions:width=1349,height=526',
            ]; 
        } 

        $request->validate($rules, [
            'name.required' => 'Nama Banner Ads Dibutuhkan!',
            'image.required' => 'Gambar Dibutuhkan!',
            'image.image' => 'File harus berupa gambar.',
            'image.mimes' => 'Gambar harus berupa file dengan jenis: jpeg, png, jpg.',
            'image.dimensions' => 'Dimensi gambar tidak valid.',
            'description.required' => 'Komentar Dibutuhkan!',
        ]);

        $image = $banner->image ?? null;

        if ($request->file('image')) {
            if( $banner->image && Storage::exists($banner->image) ) {
                Storage::delete($banner->image);
            }

            $realPath = Image::make($request->file('image')->getRealPath());
            $extension = $request->file('image')->getClientOriginalExtension();
            $origin = Image::make($realPath)->encode($extension, 75);
            $path = 'banners/' . Str::random(40).'.'.$extension;

            Storage::put($path, $origin->__toString());
            $image = $path;

           
        }

        $slug = Str::slug($request->name);
        $status = "off";

        if ($request->status == "on") {
            $status = "on";
        }


        $data = array_merge(
            $request->only('name', 'description'),
            compact('image', 'slug', 'status')
        );


        $banner->update($data);
    }
}
