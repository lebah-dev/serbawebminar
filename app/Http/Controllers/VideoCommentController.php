<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\VideoCommentResource;
use Illuminate\Http\Request;
use App\Models\{Channel, Video, Comment};
use Inertia\Inertia;
use Auth, DB, Image, Storage, Str;

class VideoCommentController extends Controller
{
    public function APIIndex($slug)
    {
        $video = Video::where('slug', $slug)->firstOrFail();
        $comments = $video->videoComments()->with(['user', 'parents'])->whereNull('parent_id')->get();

        return VideoCommentResource::collection($comments);

    }
}
