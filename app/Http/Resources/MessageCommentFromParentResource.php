<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageCommentFromParentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_message' => $this->id,
            'avatar' => $this->user->user_avatar ? $this->user->user_avatar->getImage() : asset('images/resources/user-img.png'),
            'username' => $this->user->name,
            'created_at' => $this->created_at,
            'likes_count' => $this->countLike(),
            'dislikes_count' => $this->countDislike(),
            'comments_count' => $this->comments_count,
            'content' => $this->content,
        ];
    }
}
