<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\{ File, Http };


class VideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'video_type' => $this->video_type,
            'title' => $this->title,
            'slug' => $this->slug,
            'path' => $this->video_type == 'upload' ? route('storage.retrieve', encrypt($this->path)) : $this->path,
            'about' => $this->about,
            'thumbnail' => $this->video_type == 'upload' ? route('storage.retrieve', encrypt($this->thumbnail)) : $this->thumbnail,
            'privacy_setting' => $this->privacy_setting,
            'views_count' => $this->video_type == 'upload' ? $this->views_count : $this->countViewYoutube($this->path),
            'channel' => $this->createBy->channel->name,
            'channel_id' => $this->createBy->channel->id,
            'created_at' => $this->created_at,
            'duration' => convert_time($this->duration),
            'url_video' => route('video', [$this->createBy->channel->links, $this->slug]),
            'avatar_user' => $this->createBy->user_avatar ? $this->createBy->user_avatar->getImage() : asset('images/resources/user-img.png'),
            'posted_at' => short_date($this->created_at),
            'isLikeVideo' => Auth::check() ? (Auth::user()->userLikeVideos()->where('video_id', $this->id)->where('like', 'like')->first() ? true : false) : false,
            'isDislikeVideo' => Auth::check() ? (Auth::user()->userLikeVideos()->where('video_id', $this->id)->where('like', 'dislike')->first() ? true : false) : false,
            'countLike' => $this->countLike(),
            'countDislike' => $this->countDislike(),
            'countSubscribe' => $this->countSubscribe(),
            'isSubscribe' => Auth::check() ? (Auth::user()->userSubscribes()->where('channel_id', $this->createBy->channel->id)->first() ? true : false) : false,
            'url_channel' => route('channel', $this->createBy->channel->links),
            'url_youtube' => $this->getLinkYoutube($this->path),
        ];
    }

    public function countViewYoutube($videoId)
    {
        $youtubeKey = config('services.youtube.key');
        $response = Http::get('https://www.googleapis.com/youtube/v3/videos?id='.$videoId.'&key='.$youtubeKey.'&part=snippet,contentDetails,statistics,status')->json();

        return number_format($response['items'][0]['statistics']['viewCount']);
    }

    public function getLinkYoutube($videoId)
    {
        $link = 'https://www.youtube.com/watch?v=' . $videoId;

        return $link;
    }
}
