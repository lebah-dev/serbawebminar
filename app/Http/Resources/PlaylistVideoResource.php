<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PlaylistVideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'video_type' => $this->video_type,
            'title' => $this->title,
            'slug' => $this->slug,
            'path' => $this->video_type == 'upload' ? asset('storage/video/'.$this->path) : $this->path,
            'about' => $this->about,
            'thumbnail' => $this->video_type == 'upload' ? config('image.video_thumbnail') . $this->thumbnail : $this->thumbnail,
            'privacy_setting' => $this->privacy_setting,
            'views_count' => $this->views_count,
            'channel' => $this->createBy->channel->name,
            'channel_id' => $this->createBy->channel->id,
            'created_at' => $this->created_at,
            'duration' => convert_time($this->duration),
            'url_video' => route('video', [$this->createBy->channel->links, $this->slug])
        ];
    }
}
