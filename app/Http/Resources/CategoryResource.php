<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array(
            'name' => $this->name,
            'url' => route('category.show', $this->slug),
            'slug' => $this->slug,
            'image' => $this->getImage(),
            'count_videos' => $this->videos->isNotEmpty() ? $this->videos->count() : 0,
        );
    }
}
