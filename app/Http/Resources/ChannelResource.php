<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class ChannelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'cover' => $this->cover_image ?  route('storage.retrieve', encrypt($this->cover_image))  : asset('images/resources/ch1.jpg'),
            'countFollowers' => $this->userSubscribes->count(),
            'url' => route('channel', $this->links),
            'avatar_user' => $this->user->user_avatar ? $this->user->user_avatar->getImage() : asset('images/resources/user-img.png'),
            'videos' => VideoResource::collection($this->user->videos()->orderBy('created_at', 'desc')->get()),
            'isSubscribe' => Auth::check() ? (Auth::user()->userSubscribes()->where('channel_id', $this->id)->first() ? true : false) : false
        ];
    }
}
