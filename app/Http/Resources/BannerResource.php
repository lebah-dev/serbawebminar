<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data =[
            'name' => $this->name,
            'url' => route('banner.show', [$this->slug]),
            'image' => route('storage.retrieve', encrypt($this->image)),
            'description' => $this->description,
            'posted_at' => short_date($this->created_at),
        ];

        return $data;
        // return parent::toArray($request);
    }
}
