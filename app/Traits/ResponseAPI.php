<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Str;

trait ResponseAPI
{
	public function sendResponse(String $message, Int $status_code = 200, $payloads = NULL)
    {
        $response = [
            'status' => $status_code === 200 ? 'success' : 'error',
            'status_code' => $status_code,
            'message' => $message,
            'response_time' => (microtime(true) - LARAVEL_START),
            'data' => $payloads ? $payloads : NULL
        ];

        return response()->json($response, $status_code);
    }
}
