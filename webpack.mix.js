const mix = require('laravel-mix');
const path = require('path')
const Dotenv = require('dotenv-webpack')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
.sass('resources/sass/app.scss', 'public/css')
.webpackConfig({
  output: { chunkFilename: 'js/[name].js?id=[chunkhash]' },
  resolve: {
    alias: {
      ziggy: path.resolve('vendor/tightenco/ziggy/dist'),
      vue$: 'vue/dist/vue.runtime.esm.js',
      '@': path.resolve('resources/js'),
    },
  },
  plugins: [
   new Dotenv()
  ]
})
.scripts([ 
  'public/assets/front/js/jquery.min.js', 
  'public/assets/front/js/popper.js', 
  'public/assets/front/js/bootstrap.min.js', 
  'public/assets/front/js/flatpickr.js', 
  'public/assets/front/js/script.js' ], 'public/js/template.js')
.version()
.sourceMaps();
