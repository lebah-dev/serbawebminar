# VIDEO APPS - SERBAWEBMINAR

This application is for sharing Webminar Video.

## Technology Stack

+ Laravel 8
+ MySQL / MariaDB
+ InertiaJS
+ VueJS

## Local Installment

1. clone this repo
2. open project dir
3. `composer install`
4. `cp .env.example .env`
5. `php artisan key:generate`
6. prepare a mysql/mariadb database
7. update `.env` file
8. `php artisan migrate`
9. `npm install`
10. `npm run dev` for develop / `npm run prod` for production
11. `php artisan serve`

## Cpanel Deployment

1. Build app based on cpanel requirements
2. Zip vendor directory
3. Setup .env based on cpanel url
4. Generate ziggy router
5. Push to master
6. On cpanel pull from master
7. Extract vendor.zip
8. Set up .env
9. Run it