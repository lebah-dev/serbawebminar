<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',
    'avatar' => env('APP_URL') . '/storage/avatar-thumb/',
    'channel' => env('APP_URL') . '/storage/channel/',
    'video_thumbnail' => env('APP_URL') . '/storage/video_thumbnail/',
    'banner' => env('APP_URL') . '/storage/banners/',
    'video' => env('APP_URL') . '/storage/video/'

];
