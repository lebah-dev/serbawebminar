<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
use App\Http\Controllers\Customer;
use App\Http\Controllers\StorageController;

Route::get('/', [Controllers\WelcomeController::class, 'index'])->name('welcome');
Route::get('/login', [Controllers\Auth\LoginController::class, 'index'])->name('login');
Route::post('/login', [Controllers\Auth\LoginController::class, 'login']);
Route::post('/resend-verifyemail', [Controllers\Auth\LoginController::class, 'resendVerifyEmail']);

Route::get('/register', [Controllers\Auth\RegisterController::class, 'index'])->name('register');
Route::post('/register', [Controllers\Auth\RegisterController::class, 'store']);
Route::get('/email/confirmation/{email}', [Controllers\Auth\RegisterController::class, 'confirmEmail'])->name('confirm.email');

Route::get('/home', [Controllers\HomeController::class, 'index'])->name('home');

Route::get('/password/reset', [Controllers\Auth\ForgotPasswordController::class, 'showLinkRequestForm']);
Route::post('/password/email', [Controllers\Auth\ForgotPasswordController::class, 'postEmail']);

Route::get('/password/reset/{token}', [Controllers\Auth\ResetPasswordController::class, 'showResetForm'])->name('password.reset');
Route::post('/password/reset', [Controllers\Auth\ResetPasswordController::class, 'reset']);

Route::get('/authorize/{social}', [Controllers\Auth\SocialAuthorizeController::class, 'redirectToProvider']);
Route::get('/authorize/{social}/callback', [Controllers\Auth\SocialAuthorizeController::class, 'handleProviderCallback']);

Route::resource('/category', Controllers\CategoryController::class)->only('index', 'show');
Route::get('/api/category', [Controllers\CategoryController::class, 'APIIndex']);


Route::group(['prefix' => 'customer'], static function () {
    Route::group(['middleware' => 'auth'], static function() {
        Route::post('/logout', [Controllers\Auth\LoginController::class, 'logout'])->name('logout');
        Route::get('/', [Customer\MyAccountController::class, 'index'])->name('customer.account');
        Route::get('/password', [Customer\PasswordController::class, 'index'])->name('customer.password');
        Route::post('/password', [Customer\PasswordController::class, 'store']);

        Route::get('/api/avatar', [Customer\AvatarController::class, 'APIAvatar']);
        Route::post('/avatar', [Customer\AvatarController::class, 'store']);

        Route::get('/video', [Customer\VideoController::class, 'index'])->name('customer.video');
        Route::get('/video/watch/{video:slug}', [Customer\VideoController::class, 'watch'])->name('customer.video.watch');
        Route::get('/video/{video:slug}', [Customer\VideoController::class, 'edit'])->name('customer.video.edit');
        Route::put('/video/{video:slug}/update', [Customer\VideoController::class, 'update'])->name('customer.video.update');
        Route::delete('/video/{video:id}/delete', [Customer\VideoController::class, 'destroy']);
        Route::post('/video', [Customer\VideoController::class, 'store']);

        Route::get('/channel', [Customer\ChannelController::class, 'index'])->name('customer.index.channel');
        Route::get('/channel/create', [Customer\ChannelController::class, 'create'])->name('customer.create.channel');
        Route::post('/channel', [Customer\ChannelController::class, 'store']);
        Route::get('/channel/{channel}/edit', [Customer\ChannelController::class, 'edit'])->name('customer.edit.channel');
        Route::put('/channel/{channel}/update', [Customer\ChannelController::class, 'update'])->name('customer.update.channel');

        Route::post('/like', [Customer\UserLikeVideoController::class, 'store']);
        Route::post('/comment', [Customer\UserVideoCommentController::class, 'storeNewComment']);
        Route::post('/comment/reply', [Customer\UserVideoCommentController::class, 'storeReplyComment']);
        Route::post('/like-comment', [Customer\UserVideoCommentController::class, 'storeResponsComment']);
        Route::post('/subscribe', [Customer\UserSubscribeController::class, 'store']);

        Route::get('/playlist', [Customer\UserPlaylistController::class, 'index'])->name('customer.playlist');
        Route::get('/api/playlist', [Customer\UserPlaylistController::class, 'APIIndex']);
        Route::post('/playlist', [Customer\UserPlaylistController::class, 'store']);
        Route::post('/playlist-video', [Customer\UserPlaylistController::class, 'storeVideoPlaylist']);
        Route::delete('/playlist/{playlist}/{video}/delete', [Customer\UserPlaylistController::class, 'destroy']);

        Route::get('/notifications', [Customer\UserNotificationController::class, 'getUnreadNotification']);
        Route::get('/notifications/mark-read-all', [Customer\UserNotificationController::class, 'markReadAllNotif']);
        Route::get('/notifications/{id}', [Customer\UserNotificationController::class, 'markReadNotif']);
        
    });
    
});


Route::get('/watch/{channel}/{video}', Controllers\VideoController::class)->name('video');
Route::get('/watch/{channel}', Controllers\ChannelController::class)->name('channel');
Route::get('/api/video/{video}', [Controllers\VideoCommentController::class, 'APIIndex']);
Route::post('/api/search/video', [Controllers\ChannelController::class, 'search']);


Route::get('/search', [Controllers\SearchController::class, 'index'])->name('search.index');
Route::get('/banner/{slug}', [Controllers\BannerController::class, 'show'])->name('banner.show');


Route::get('/retrieve-storage/{encryptedPath}', [StorageController::class, 'retrieve'])->name('storage.retrieve');

Route::get('/about', [Controllers\Footer\AboutController::class, 'index'])->name('footer.about.index');
Route::get('/about-use', [Controllers\Footer\AboutUsController::class, 'index'])->name('footer.about-use.index');
Route::get('/media-sosial', [Controllers\Footer\MediaSosialController::class, 'index'])->name('footer.media-sosial.index');
Route::get('/pusat-bantuan', [Controllers\Footer\PusatBantuanController::class, 'index'])->name('footer.pusat-bantuan.index');


