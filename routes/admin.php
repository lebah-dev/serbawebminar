<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin;


Route::get('/login', [Admin\Auth\LoginController::class, 'index'])
		->name('login.index');
Route::post('/login', [Admin\Auth\LoginController::class, 'login'])
		->name('login');
Route::get('/resend-verifyemail/{email}', [Admin\Auth\LoginController::class, 'resendVerifyEmail'])
		->name('resend-verifyemail');

Route::get('/password/email', [Admin\Auth\ForgotPasswordController::class, 'index'])
		->name('password.email.index');
Route::post('/password/email/send', [Admin\Auth\ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email.send');

Route::get('/password/reset/{token}', [Admin\Auth\ResetPasswordController::class, 'showResetForm'])->name('password.reset');
Route::post('/password/reset', [Admin\Auth\ResetPasswordController::class, 'reset'])->name('password.reset.update');


Route::group(['middleware' => 'admin'], static function() {
	Route::get('/', function() {
		return redirect()->route('admin.dashboard.index');
	});
	
	Route::post('/logout', [Admin\Auth\LoginController::class, 'logout'])
		->name('logout');

	Route::get('/dashboard', [Admin\DashboardController::class, 'index'])
			->name('dashboard.index');

	Route::resource('/banner', Admin\BannerController::class)->except('show');
	Route::get('/data/banner', Admin\DataTable\BannerController::class)
			->name('banner.data');

	Route::resource('/category', Admin\CategoryController::class)->except('show');
	Route::get('/data/category', Admin\DataTable\CategoryController::class)
			->name('category.data');

	Route::resource('/comments', Admin\CommentController::class)->except('show', 'index');
	Route::delete('/comments/{id}/delete-reply', [Admin\CommentController::class, 'destroyReply'])
			->name('comments.destroyReply');
	Route::get('/data/comments', Admin\DataTable\CommentController::class)
			->name('comments.data');

	Route::get('/video/from-channel', [Admin\VideoController::class, 'createFromChannel'])
			->name('video.from-channel');
	Route::get('/video/video-from-channel', [Admin\VideoController::class, 'videoFromChannelIndex'])
			->name('video.video-from-channel.index');
	Route::post('/video/video-from-channel-check', [Admin\VideoController::class, 'videoFromChannel'])
			->name('video.video-from-channel');
	Route::post('/video/more-video-from-channel/', [Admin\VideoController::class, 'moreVideoFromChannel'])
			->name('video.more-video-from-channel');
	Route::post('/video/store-video-from-channel', [Admin\VideoController::class, 'storeVideoFromChannel'])
			->name('video.store-video-from-channel');

	Route::post('/video/search-video-from-channel', [Admin\VideoController::class, 'searchVideoFromChannel'])
			->name('video.search.video-from-channel');
	Route::get('/video/search-video-from-channel', [Admin\VideoController::class, 'searchVideoFromChannelIndex'])
			->name('video.search.video-from-channel.index');
	Route::post('/video/search-more-video-from-channel/', [Admin\VideoController::class, 'searchMoreVideoFromChannel'])
			->name('video.search.more-video-from-channel');
	// Route::post('/video/search-store-video-from-channel', [Admin\VideoController::class, 'searchStoreVideoFromChannel'])
	// 		->name('video.search.store-video-from-channel');


	Route::resource('/video', Admin\VideoController::class);
	Route::get('/data/video', Admin\DataTable\VideoController::class)
			->name('video.data');

	Route::resource('/playlist', Admin\PlaylistController::class);
	Route::get('/data/playlist', Admin\DataTable\PlaylistController::class)
			->name('playlist.data');

	Route::resource('/channel', Admin\ChannelController::class)->except('show');
	Route::get('/data/channel', Admin\DataTable\ChannelController::class)
			->name('channel.data');

	Route::resource('/users', Admin\UserController::class);
	Route::get('/data/users', Admin\DataTable\UserController::class)
			->name('users.data');

	Route::get('/profile', [Admin\ProfilController::class, 'index'])
			->name('profil.index');
	Route::get('/profile/edit', [Admin\ProfilController::class, 'edit'])
			->name('profil.edit');
	Route::put('/profile/{id}/update', [Admin\ProfilController::class, 'update'])
			->name('profil.update');
	Route::put('/profile/{id}/change-password', [Admin\ProfilController::class, 'changePassword'])
			->name('profil.changePassword');

	Route::resource('/admins', Admin\AdminController::class);
	Route::get('/data/admins', Admin\DataTable\AdminController::class)
			->name('admins.data'); 
});